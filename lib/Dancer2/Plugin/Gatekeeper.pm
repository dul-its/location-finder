package Dancer2::Plugin::Gatekeeper;

use Dancer2::Plugin;
use Data::Dumper;
use List::Util qw(any);

has admin_group => (
  is              => 'ro',
	from_config     => sub { '' },
);

plugin_keywords 'user_can_edit';

sub BUILD {
	my $plugin = shift;
}

sub user_can_edit {
  my $plugin = shift;
  
  #my @ismemberof = split(';', $ENV{ismemberof});
  #any { $_ eq $plugin->admin_group } @ismemberof;
  0;
}

1;
__END__
