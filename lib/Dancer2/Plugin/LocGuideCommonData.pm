package Dancer2::Plugin::LocGuideCommonData;

use strict;
use warnings;
use Carp;
use Moo;
use Dancer2::Plugin;
#use Dancer2::Plugin::Database;
use Number::Format qw(:subs);
use Data::Dumper;


has dancer2_plugin_database => (
	is   => 'ro',
	lazy => 1,
	default =>
		sub { $_[0]->app->with_plugin('Dancer2::Plugin::Database') },
	handles  => { plugin_database => 'database' },
	init_arg => undef,
);

=head2 database

The connected L</plugin_database> using L</db_connection_name>.

=cut

has database => (
	is => 'ro',
	lazy => 1,
	default => sub {
		my $self = shift;
		$self->plugin_database($self->db_connection_name);
	},
);

has db_connection_name => (
	is => 'ro',
);

register sublibrary => sub {
	my $dsl = shift;
	my $scode = shift;

	my $sql = '';
	my $sth = undef;

	# fetch the sublibrary data
	$sql = "SELECT * FROM locationguide_sublibrary WHERE code = ?";
	$sth = $dsl->database->prepare( $sql );
	$sth->execute( $scode );
	my $sublibrary = $sth->fetchrow_hashref;

	# next, fetch the Collection Groups
	$sql = "SELECT * FROM locationguide_collectiongroup WHERE sublibrary_id = ?";
	$sth = $dsl->database->prepare( $sql );
	$sth->execute( $sublibrary->{id} );
	$sublibrary->{collectiongroups} = {};
	my $cgroups = $sth->fetchall_arrayref( {} );

	$sql = "SELECT * FROM locationguide_collection WHERE collection_group_id = ?";
	$sth = $dsl->database->prepare( $sql );
	my $lmap_sql = "SELECT * FROM locationguide_locationmap WHERE collection_group_id = ?";
	my $lmap_sth = $dsl->database->prepare( $lmap_sql );
	map {
		$sublibrary->{collectiongroups}->{$_->{id}} = $_;

		# fetch the collections for each collection group
		# because a collection can belong to either a collection group
		# or directly under a sublibrary
		$sth->execute($_->{id});
		$sublibrary->{collectiongroups}->{$_->{id}}{collections} = $sth->fetchall_arrayref({});

		$lmap_sth->execute($_->{id});
		$sublibrary->{collectiongroups}->{$_->{id}}{locationmaps} = $lmap_sth->fetchall_arrayref({});
		
	} @{$cgroups};

	$sql = "SELECT * FROM locationguide_collection WHERE sublibrary_id = ?";
	$sth = $dsl->database->prepare( $sql );
	$sth->execute($sublibrary->{id});
	$sublibrary->{collections} = $sth->fetchall_arrayref({});

	return $sublibrary;
};

register dul_external_links => sub {
	my $dsl = shift;
	
	# fetch the external links
	my $sql = "
		SELECT *, locationguide_externallink.title as label FROM locationguide_externallink
	";
	my $extlink_sth = $dsl->database->prepare( $sql );
	$extlink_sth->execute;
	my $external_links = $extlink_sth->fetchall_arrayref( {} );

	return $external_links;
};

register dul_genericmessages => sub {
	my $dsl = shift;
	
	# fetch the external links
	my $sql = "
		SELECT * FROM locationguide_genericmessage
	";
	my $sth = $dsl->database->prepare( $sql );
	$sth->execute;
	my $genericmessages = $sth->fetchall_arrayref( {} );

	return $genericmessages;
};

register dul_areamaps => sub {
	my $dsl = shift;

	# fetch the area maps
	my $sql = "
		SELECT *, CONCAT_WS(' - ', building, area) as label FROM locationguide_areamap
	";
	my $areamap_sth = $dsl->database->prepare( $sql );
	$areamap_sth->execute;
	my $areamaps = $areamap_sth->fetchall_arrayref( {} );

	return $areamaps;
};

register dul_sublibraries => sub {
	my $dsl = shift;

	# fetch the area maps
	my $sql = "SELECT * FROM locationguide_sublibrary where collgroup_choice = 1";
	my $sth = $dsl->database->prepare( $sql );
	$sth->execute;
	my $sublibraries = $sth->fetchall_arrayref( {} );

	return $sublibraries;
};

register dul_collectiongroups => sub {
	my $dsl = shift;

	my $sql = "
		SELECT CONCAT_WS(' - ', s.label, c.description) as label, c.id 
		FROM locationguide_collectiongroup c 
		LEFT JOIN locationguide_sublibrary s ON c.sublibrary_id = s.id 
		WHERE s.collgroup_choice = 1 
		ORDER BY s.label, c.description
		";
	my $sth = $dsl->database->prepare( $sql ) or die $dsl->database->errstr;
	$sth->execute;
	my $collectiongroups = $sth->fetchall_arrayref( {} );
	return $collectiongroups;
};

register dul_areamap_buildings => sub {
	my $dsl = shift;

	my $sql = "SELECT DISTINCT building AS label FROM locationguide_areamap";
	my $sth = $dsl->database->prepare( $sql );
	$sth->execute;
	my $buildings = $sth->fetchall_arrayref({});
	map {
		$_->{id} = $_->{label}
	} @{$buildings};
	$dsl->debug( Dumper( $buildings ) );
	return $buildings;
};

register is_favorite => sub {
	my ($dsl, $content_type, $object_id) = @_;
	#$dsl->debug( Dumper( $dsl->app ));
	my $auth_user = $dsl->app->{request}->{vars}->{'auth_user'};

	my $sql = "
		SELECT * FROM django_favorites f 
		LEFT JOIN django_content_type ct ON ct.id = f.content_type_id 
		WHERE ct.name = ? AND f.object_id = ? AND f.user_id = ?
		LIMIT 0,1
		";
	my $sth = $dsl->database->prepare( $sql );
	$sth->execute( $content_type, $object_id, $auth_user->{id} );
	my $row = $sth->fetchrow_hashref();

	return $row ne undef;
};

register record_admin_log => sub {
	my ($dsl, $user_id, $content_type_id, $sublibrary_id, $sublibrary_label, $log_message) = @_;

	my $sql = "
		INSERT INTO django_admin_log VALUES ('', CURRENT_TIMESTAMP, ?, ?, ?, ?, 1000, ?)
	";
	my $sth = $dsl->database->prepare( $sql );
	$sth->execute($user_id, $content_type_id, $sublibrary_id, $sublibrary_label, $log_message);

	#my $h = $dsl->database->prepare( "SELECT * FROM locationguide_sublibrary" );
	#$h->execute();
	#my $sublibraries = $h->fetchall_arrayref( {} );
	#$dsl->debug( Dumper( $sublibraries ) );

	#$dsl->debug( Dumper( $dsl->database ) );
	my $app = $dsl->app;

	#$dsl->debug( Dumper( $dsl ) );
	#$dsl->debug( Dumper( $dsl->app->plugins ));
};

register_plugin;
1;
