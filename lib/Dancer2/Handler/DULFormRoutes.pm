package Dancer2::Handler::DULFormRoutes;
use strict;
use warnings;
use Dancer2::Plugin::Database;
use Moo;
use Carp;
use Data::Dumper;
use Number::Format qw(:subs);
use DateTime;
use Env;

with qw<
    Dancer2::Core::Role::Handler
    Dancer2::Core::Role::StandardResponses
>;

sub register {
	my ( $self, $app ) = @_;

	print STDERR "here\n";
	return unless $app->config->{dul_form_route};
	#$app->debug( "registering FORM ROUTES" );

	$app->add_route(
		method => $_,
		regexp => $self->regexp,
		code	 => $self->code,
	) for $self->methods;
}

sub code {
	sub {
		my $app			= shift;
		my $prefix	= shift;

		return 'hi there';
	};
}

sub methods { qw(get post) }

sub regexp { '/butter'}

1;