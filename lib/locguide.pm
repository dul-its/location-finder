package locguide;
use strict;
use Dancer2;
use Dancer2::Handler::DULFormRoutes;
use Dancer2::Plugin::LocGuideCommonData;
use Dancer2::Plugin::Database;
use Dancer2::Plugin::Ajax;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Gatekeeper;
use Dancer2::Serializer::JSON;
use Carp;
use Data::Dumper;
use DateTime;
use Env;
use Number::Format qw(:subs);

# Each of these modules reside under 
# lib/locguide/
# --
# When creating a module, be sure to add it here --
# or else, the app won't be able to find your code.
use locguide::ExternalLink;
use locguide::LocationMap;
use locguide::CollectionGroup;
use locguide::Collection;
use locguide::AreaMap;
use locguide::Sublibrary;
use locguide::GenericMessage;
use locguide::Typeahead;
use locguide::MapInfo;

our $VERSION = '2.00';

prefix '/admin' => sub {
  get '/' => sub {
    template 'admin/index';
  };
};

hook before => sub {
  my $auth_user;
  my $netid;

  # Return (no further processing) when we encounter the following paths:
  # - /mapinfo
  # - /api/...
  return if request->path =~ /^\/(api|mapinfo|health|logout)/;

  var previous_path => request->header('referer');

  # store the 'object' id as a 'var' for easy access 
  # by the target route handler
  # debug request->headers;

  # This is the (entity) id of whichever item is requested
  # (collection, areamap, externallink, etc)
  # The idea is to make it accessible for all affected routes (below)
  var id => route_parameters->get('id') || undef;

  if (!session('user')) {
    if (request->header('x-forwarded-user')) {
      # we, at least, have an authenticated user
      my $access_token = request->header('x-forwarded-access-token');
      if ( $access_token ) {
        my $curl = sprintf(
          "curl -X POST --data 'token=%s' --user '%s:%s' %s",
          $access_token,
          config->{oauth_oidc_client_id},
          config->{oauth_oidc_client_secret},
          config->{oauth_oidc_introspect_url}
        );
        my $resp = `$curl`;
        debug Dumper $resp;
        $auth_user = from_json($resp);
        session user => $auth_user;
        session access_token => $access_token;
        # session user => request->header('x-forwarded-user');
      } else {
        session user => {
          'given_name' => "Library Staff"
        };
      }
    }
  } else {
    $auth_user = session('user');
  }

  var can_delete => user_can_edit();

  # EVERYTHING below here is to be ignored when 
  # the request is 'ajax'-based, so as to not 
  # inadvertently clobber session variables below
  return if request->is_ajax;
 
  if (request->path =~ /^\/collectiongroups\/?$/) {
    my @filter_sublibraries = schema->resultset('LocationguideSublibrary')->search( undef, { order_by => 'code' } );
    var filter_sublibraries => \@filter_sublibraries;
  }

  if (request->path =~ /^\/collectiongroup\//) {
    my @sublibraries = schema->resultset('LocationguideSublibrary')->search( { collgroup_choice => 1 } );
    var sublibraries => \@sublibraries;
  }
  if (request->path =~ /^\/collections\/?$/) {
    # gather common data used by "collection" routes
    my @filter_sublibraries = schema->resultset('LocationguideSublibrary')->search( undef, { order_by => 'code' } );
    my @filter_collgroups = schema->resultset('LocationguideCollectiongroup')->search( undef, { order_by => 'description' } );
    var filter_sublibraries => \@filter_sublibraries;
    var filter_collgroups => \@filter_collgroups;
  }
  if (request->path =~ /^collection\/?/) {
  }

  if (request->path =~ /^\/locationmap\//) {
    my @collectiongroups = schema->resultset('LocationguideCollectiongroup')->all;

    my $areamap_filter = undef;
    my $building_filt = request->query_parameters->get('building');
    if ($building_filt) {
      $areamap_filter = { 'building' => $building_filt };
    }
    my @areamaps = schema->resultset('LocationguideAreamap')
      ->search( $areamap_filter, { order_by => { -asc => [qw/building area/] } } );
    my @externallinks = schema->resultset('LocationguideExternallink')->all;

    var collectiongroups => \@collectiongroups;
    var areamaps => \@areamaps;
    var externallinks => \@externallinks;     
  }
  
  my $request_path = request->path;
  $request_path =~ s/\///;
  
  session current_view => $request_path if !session('current_view');
  if (session('current_view') ne $request_path) {
    session current_view => $request_path;
    session page_settings => undef;
  }
  
  my $page_settings = setting('ui')->{$request_path};
  
  # merge the default settings with what is sessioned.
  $page_settings = { %$page_settings, %{ session('page_settings') } } if session('page_settings');
  
  # these two variables are only defined when data pagination 
  # is needed
  if ($page_settings) {
    var page => query_parameters->get('p') || session('page') || 1;
    var rows => query_parameters->get('r') || $page_settings->{rows_per_page};
        
    # Now that the session snafu has been resolved...
    # resume work on orderby / sort
    if (my $sort_field = query_parameters->get('s')) {   
      my ($stored_sort_dir, $stored_sort_field) = %{$page_settings->{order_by}};
      
      if ($sort_field eq $stored_sort_field) {
        $stored_sort_dir = ($stored_sort_dir eq '-asc') ? '-desc' : '-asc';
      } else {
        $stored_sort_dir = '-asc';
      }
      $stored_sort_field = $sort_field;
      $page_settings->{order_by} = { $stored_sort_dir => $stored_sort_field };
    }
    
    # save the page settings (hopefully with the updated sort settings)
    session page_settings => $page_settings;
  }

  # NOTED:
  # developer is aware of variable duplication here.
  # this var will be available for the route handler
  var page_settings => $page_settings;
  
  var auth_user => $auth_user;
  var body_class => [];

  my $now = DateTime->now(time_zone => 'America/New_York');
  var now_stamp => $now->strftime("%Y-%m-%d %H:%M:%S");

  if (request->path =~ /^\/locationmaps/) {
    my @context_buildings = ('Bostock', 'Perkins', 'Lilly', 'MedCenter');
    var nav_context_buildings => \@context_buildings;
  }
};

hook before_template_render => sub {
  my $tokens = shift;

  debug sprintf("@@@ PREVIOUS PATH ==> %s", vars->{previous_path});

  $tokens->{auth_user} = var('auth_user');
  $tokens->{body_class} = join ', ', @{vars->{body_class}};
  
  # Calculate Pager settings (when necessary)
  # This block executes only when a "list" route passes a 
  # 'pager' hash key in it's "template" call...
  if ($tokens->{pager}) {
    use List::Util qw[min max];
    
    # TIP SOURCE:
    # https://stackoverflow.com/questions/2342447/custom-paging-algorithm-to-calculate-pages-to-display
    my $Z = $tokens->{pager}->current_page;
    my $Q = var('page_settings')->{pager}->{pages_per_chapter};
    my $r = int(($Z-1)/$Q);
    my $begin = $Q*$r + 1;
    my $end = min($Q*($r+1), $tokens->{pager}->last_page);
        
    $tokens->{pager}->{page_begin} = $begin;
    $tokens->{pager}->{page_end} = $end;
    $tokens->{pager}->{working_url} = sprintf("%s%s", request->uri_base, request->path);

    # Preserve the previous query string, but remove any "p" parameters
    my @qstr = grep {
      my @z = split "=", $_;
      $z[0] ne "p";
    } split "&", request->query_string;
    debug Dumper request->query_string;
    debug Dumper \@qstr;
    $tokens->{pager}->{query_string} = join "&", @qstr;
    
    # any non-zero value will satisy 'truth' here
    $tokens->{pager}->{show_page_one} = $r;
    
    $tokens->{pager}->{current_chapter} = $r + 1;
  }
  
  # determine css display classes for sortable fields
  #
  # FOR this to work correctly, you must add a "sortable_fields"
  # list for the affected page 'view' (e.g. sublibraries, collections, collectiongroups, etc)
  # in 'config.yml'
  # - 
  # From there, the appropriate template needs to "<% INCLUDE 'sortable_column.tt' field_title='some-title', ikey='some-db-field' %>
  # to the appropriate <TH> cell.
  # 
  # @see views/sublibraries.tt and views/collections.tt
  if (var('page_settings') && defined var('page_settings')->{order_by}) {
    my $page_settings = var('page_settings');

    my ($stored_sort_dir, $stored_sort_field) = %{$page_settings->{order_by}};
    
    if (defined $page_settings->{sortable_fields}) {
      my $display_sort_ico = {};
      foreach my $f ( @{$page_settings->{sortable_fields}} ) {
        $display_sort_ico->{$f} = ($f eq $stored_sort_field) ? (($stored_sort_dir eq '-asc') ? 'fa-sort-up' : 'fa-sort-down') : 0;
      }
      $tokens->{display_sort_ico} = $display_sort_ico;
    }
  }

  $tokens->{use_masonry} = 1 if vars->{use_masonry};
};

hook after_template_render => sub {
  session->delete('show_validation_alert');
  session->delete('validation_alert_title');
  session->delete('validation_alert_message');
};

# 'AFTER' hook
# Currently using this hook to clear any defined session variables
# at the end of all 'response' processing.
hook after => sub {
};

hook before_error => sub {
  my $error = shift;
  my @keys = keys %{$error};
  #foreach my $k (keys %{$error}) {
  #  debug $error->{$k};
  #}
  debug "====================";
  debug Dumper \@keys;
  debug $error->{message} if $error->{message};
  debug $error->{show_errors} if $error->{show_errors};
  debug "====================";
  debug $error->{response} if $error->{response};
  debug "====================";
  debug "Is AJAX" if $error->{app}->{request}->is_ajax;
  #debug $error->{response} if $error->{response};
  #debug $error->{exception} if $error->{exception};
  #debug $error->{type} if $error->{type};

  return 0 if $error->{app}->{request}->is_ajax;
  
  # We're using HTTP Status Code 422 to indicate 'data validation' errors
  # https://www.bennadel.com/blog/2434-http-status-codes-for-invalid-data-400-vs-422.htm
  # --
  # end result here is to bypass the default '422.tt' template rendering
  # and instead redirect to the previous page, displaying an error
  if ($error->{status} eq '422') {
    # data validation error
    my $path = $error->{app}->{request}->path;
    my (undef, $view, $id) = split /\//, $path;
    debug sprintf("%s / %s", $view, $id);

    session show_validation_alert => 1;
    session validation_alert_title => "Invalid Data Entry";
    session validation_alert_message => $error->{message};
    
    redirect $error->{app}->{request}->uri_for($path, {});
  }
};

hook after_error => sub {
  my $response = shift;

};
####### END OF HOOKS ##################

####### ROUTES start here #########
get '/' => sub {
  # fetch the area map entities and allow that to be 
  # the starting point for the end-user.
  push @{vars->{body_class}}, 'dashboard';

  my $areamap_rs = schema->resultset('LocationguideAreamap')
    ->search(undef, {
      columns => [ 'building' ],
      group_by => [qw/ me.building /],
      order_by => [ { -asc => 'me.building' } ]
    });
  my @buildings = $areamap_rs->all;
  map { 
    my @a = schema->resultset('LocationguideAreamap')
      ->search( {
        building => $_->building
        },
        { order_by => 'area' }
      );
    $_->{areamaps} = \@a; 
  } @buildings;
  
  my @areamaps = schema->resultset('LocationguideAreamap')
    ->search(undef, {
      order_by => [ { -asc => 'me.building' } ]
    });

  my $object_trigger;

  my $building_collapse_target = session('building_target');
  debug "front page - " . $building_collapse_target;

  template 'dashboard', { 
    activepage => 'dashboard',
    buildings => \@buildings,
    areamaps => \@areamaps,
    building_active => $building_collapse_target,
  };
};

# Respond to "map location service" requests from 
# DUL's discovery web app (primary) and possibly 
# other locations
# 
# This route is only recognized when called from an
# AJAX request.
#
# For CLI example:
# curl -d '{"collection_code":"PK7","sublibrary":"PERKN","callno":"RE36.B78 A3 2022b"}' \
#   -X POST https://yourhost/mapinfo \
#   -H 'Content-Type: application/json' \
#   --header 'X-Requested-With: XMLHttpRequest'
any ['post'] => '/mapinfo' => sub {
  debug "[ajax]/mapinfo route -- made it here!";
  my $content_type = request->header('content-type');

  my $data;
 
  # jQuery's "ajax" calls send a POST over with content-type = 'x-www-form-urlencoded'
  # this is when CRS makes the API call
  if ( $content_type =~ /x-www-form-urlencoded/ ) {
    # get data from body_parameters
    $data = body_parameters->as_hashref;
  } else {
    # get data from_json(request->body)
    $data = decode_json(request->body);
  }

  # NOTE to observing developers.
  # $data <-- this is a HASHREF (pointer to a hash)
  # my $data = from_json(request->body);
  # debug Dumper $data;

  #send_error "Invalid parameters", 500 
  #  unless ($data->{callno} && $data->{collection_code}) || $data->{sublibrary};

  # NOTE to observing developers
  # %return_data <-- this is a HASH scalar
  my %return_data = (
      server_name => config->{mapfinder}->{server_name},
      media_path => config->{locguide_mediapath},
  );

  my $o = undef;
  my $no_location_data = 0;

  # First, attempt to locate the location map by inspecting 
  # the collection code, and (optionally) a call number 
  # range.
  if ( $data->{callno} && $data->{collection_code} ) {
    $o = schema->resultset("LocationguideLocationmap")->search(
      [
        -and => [
          'locationguide_collections.code' => $data->{collection_code},
          -or => [
            { callno_range => '' },
            {
              callno_range  => { '!=', '' },
              callno_start  => { '<=', $data->{callno} },
              callno_end    => { '>=', $data->{callno} }
            },
          ],
        ]
      ],
      {
        join => { 'collection_group' => 'locationguide_collections' },
        order_by => { -asc => 'callno_priority' }
      }
    )->first;
  }

  # flag that we have no data when the above query returns 'undef' and 
  # the calling source (likely the Discovery webapp) doesn't provide 
  # a 'sublibrary' code.
  $no_location_data = !$o && !$data->{sublibrary};

  # If the first query doesn't return anything, try locating
  # a location map based on a sublibrary code.
  $o = schema->resultset("LocationguideLocationmap")->search(
    {
      'locationguide_sublibrary.code' => $data->{sublibrary}
    },
    { 
      join => 'locationguide_sublibrary'
    }
  )->first if !$o && $data->{sublibrary};

  my $areamap = $o->map || undef;
  my $external_link = $o->external_link || undef;

  $no_location_data ||= (!$areamap && !$external_link);

  if ( $no_location_data ) {
    $return_data{no_data} = 1;
    $return_data{error} = 'Unable to location map or external link';
    return to_json( \%return_data );
  }

  # Add the areamap data to the %data hash, when applicable.
  if ( $areamap ) {
    %return_data = (%return_data, $areamap->get_columns);
    $return_data{theme} = 'area-map';
  }

  if ( !$areamap && $external_link ) {
    $return_data{external_url} = $external_link->url;
    $return_data{extlink_title} = $external_link->title;
    $return_data{extlink_text} = $external_link->link_text;
    $return_data{extlink_content} = $external_link->content;

    if ($external_link->generic_msg) {
      $return_data{generic_msg_content} = $external_link->generic_msg->content;
      $return_data{generic_msg_label} = $external_link->generic_msg->label;
    } else {
      $return_data{generic_msg_content} = $external_link->content;
      $return_data{generic_msg_label} = $external_link->title;
    }
    $return_data{generic_msg_content} = $external_link->generic_msg->content if $external_link->generic_msg;
    $return_data{generic_msg_label} = $external_link->generic_msg->label if $external_link->generic_msg;
    $return_data{theme} = 'external-link';
  }
  debug Dumper \%return_data;

  # regardless of how this route was called (CRS or Books/Media)
  # make sure to set content type to 'application/json'
  content_type 'application/json';
  return to_json( \%return_data );
};

prefix '/api' => sub {
  ajax ['get'] => '/areamap/:id' => sub {
    my $id = route_parameters->get('id');
    my $areamap = schema->resultset('LocationguideAreamap')
      ->find({ id => $id });

    my $response = {};
    $response->{image} = sprintf("%s/images/media/%s", request->uri_base, $areamap->image);
    $response->{building} = $areamap->building;
    $response->{area} = $areamap->area;
    $response->{floor} = $areamap->area;

    return to_json $response;
  };

  ajax ['get'] => '/info' => sub {
    return to_json { message => 'we have info under prefix...' };
  };
};

# LOGIN route -- we land here after a 
# successful Shibboleth/SP authentication
get '/login' => sub {
};

get '/logout' => sub {
  my $access_token = session('access_token');
  return unless $access_token;

  my $curl = sprintf "curl -u '%s:%s' %s -d 'token=%s'",
    config->{oauth_oidc_client_id},
    config->{oauth_oidc_client_secret},
    config->{oauth_oidc_revoke_url},
    $access_token;
  
  my $resp = `$curl`;
  debug sprintf("/logout -- %s", $resp);

};

post '/login' => sub {
  debug "/login landed here on POST";
  debug request->env;

  delayed {
    content "Hi there";
  }
};

ajax '/roles' => sub {
  my @roles = grep {
    #debug( Dumper( $_ ) );
    $_ =~ /location-guide/;
  } @{vars->{auth_user}->{ismemberof}};

  return to_json( { roles => \@roles } );
};

ajax '/building-link-clicked' => sub {
  debug "building-link-clicked...";
  my $buildingTarget = body_parameters->get('building');
  session building_target => $buildingTarget;

  return to_json( { success => 1 } );
};

get '/health' => sub {
  return "Healthy";
};

true;

__END__

