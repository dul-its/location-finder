use utf8;
package locguide::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:XqEw++PQJ+SA+S0lWD4aig


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
