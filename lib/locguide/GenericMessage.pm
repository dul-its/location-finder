package locguide::GenericMessage;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'locguide';
use Dancer2::Plugin::LocGuideCommonData;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Deferred;

# uncomment this when AJAX processing is needed
use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;

prefix '/genericmessage' => sub {
  get '/new' => sub {
    my $resultset = schema->resultset('LocationguideGenericmessage');
    my $genericmessage = $resultset->new_result( {} );
    
    var page_title => 'New Generic Message';

	  my @externallinks = schema->resultset('LocationguideExternallink')->all;
    
    template 'genericmessage', {
      genericmessage => $genericmessage,
		  externallinks => \@externallinks,
      'g' => $genericmessage,
    };
  };
  
  post '/new' => sub {
    my $form = body_parameters->as_hashref_mixed;
    $form->{id} = undef;
    
    my $genericmessage = schema->resultset('LocationguideGenericmessage')
      ->find_or_create( $form, { key => 'primary' } );
    
    send_error( "Unable to create a new Generic Message", 500 ) if !$genericmessage;
    
    deferred success => "Successfully created new Generic Message";
    
    # return to the edit form
    redirect '/genericmessage/' . $genericmessage->id;
  };

  ajax '/:id/content' => sub {
    my $genericmessage = schema->resultset('LocationguideGenericmessage')->find( { id => var('id') } );
    send_error sprintf("Unable to find Generic Message record (id: %s)", var('id')), 404
      unless $genericmessage;

    return to_json({ content => $genericmessage->content });
  };

  get '/:id/delete' => sub {
    my $genericmessage = schema->resultset('LocationguideGenericmessage')->find( { id => var('id') } );
    send_error sprintf("Unable to find Generic Message record (id: %s)", var('id')), 404
      unless $genericmessage;
    
    send_error sprintf("Unable to delete Generic Message (id: %s)", var('id')), 500
      if !$genericmessage->delete;
      
    deferred success => "Generic Message removed successfully";
    
    redirect '/genericmessages';
  };
 
  get '/:id' => sub {
    my $genericmessage = schema->resultset('LocationguideGenericmessage')
      ->find({ id => var('id') });
    
	  if ( !$genericmessage ) {
		  send_error "Unable to locate generic message for (" . var('id') . ")", 404;
	  }
		var page_title => sprintf('Generic Message: %s', $genericmessage->label);
	  
	  my @externallinks = schema->resultset('LocationguideExternallink')->search(
	    { 
	      generic_msg_id => [ { '=' => undef }, { '!=', var('id') } ],
	    }, { order_by => 'title' } );
	  
	  template 'genericmessage', {
		  genericmessage => $genericmessage,
		  externallinks => \@externallinks,
		  'g' => $genericmessage,
	  };
  };
  
  post '/:id' => sub {
	  my $form = body_parameters->as_hashref_mixed;
	  #return to_json($form);
	  
	  my $deleted_external_links = schema->resultset('LocationguideExternallink')->search( { id => $form->{deleted_external_links} } );
	  delete $form->{deleted_external_links};
	  
	  my $assigned_external_links = schema->resultset('LocationguideExternallink')->search( { id => $form->{assigned_external_links} } );
	  delete $form->{assigned_external_links};
	  
    my $genericmessage = schema->resultset('LocationguideGenericmessage')->find( { id => var 'id' } );
    if (!$genericmessage->update( $form ) ) {
    
      # calling "send_error" will cause this route handler to stop at this point
      send_error "Unable to save changes for generic message (id = " . var('id') . ")", 500;
    }
    
    $assigned_external_links->update( { generic_msg_id => $genericmessage->id } );
    $deleted_external_links->update( { generic_msg_id => undef } );
    
    # saving/updating was successful.
    deferred success => "Your changes were saved.";
    
    # return to the edit form
    redirect '/genericmessage/' . route_parameters->get('id');
  };
};

prefix '/genericmessages' => sub {
  get '' => sub {    
	  my $genericmessages = schema->resultset('LocationguideGenericmessage')->search(
		  undef,
		  {
        page => var('page'),
        rows => var('rows'),
        order_by => var('page_settings')->{order_by},
		  }
	  );
    my $pager = $genericmessages->pager;
  
	  template 'genericmessages', {
		  genericmessages => $genericmessages,
		  pager => $pager,
		  activepage => 'genericmessages',
	  };
    
  };
};

1;

__END__


