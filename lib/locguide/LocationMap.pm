package locguide::LocationMap;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'locguide';
use Dancer2::Plugin::LocGuideCommonData;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Gatekeeper;
use Dancer2::Plugin::Deferred;

use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;

prefix '/locationmaps' => sub {

  get '/json' => sub {
    my @locationmaps = schema->resultset('LocationguideLocationmap')->all;
    
    my @data;
    map {
      push @data, { $_->get_columns };
    } @locationmaps;
    
    to_json \@data;
  };
  
  # This route references the following 'vars' created in 'hook before':
  # - var('page')
  # - var('rows')
  get '' => sub {
    
    # NOTE: see below __END__ marker for deprecated example code

    # config.yml
    # look for ui -> /locationmaps -> search_criteria
    my $crit = var('page_settings')->{search_criteria};
    
    my $locationmaps = schema->resultset('LocationguideLocationmap')
        ->search( $crit, { 
            page => var('page'),
            rows => var('rows'),
            order_by => var('page_settings')->{order_by},
          } );

    # retrieve the resultset's pager
    # (for pagination display)
    my $resultset_pager = $locationmaps->pager;
    
    var page_title => 'Call Number &amp; Collection Locations';
    push @{vars->{body_class}}, 'locationmaps';
      
    template 'locationmaps', {
      locationmaps => $locationmaps,
      pager => $resultset_pager,
    }
	};

  # show location map records, given an area map id
  # matches /locationmaps/areamap-by-id
  get '/:areamapid[Int]' => sub {
	  my $areamapid = route_parameters->get('areamapid');
	  debug $areamapid;
    my $areamap = schema->resultset('LocationguideAreamap')->find( { id => $areamapid } );

    my $locationmaps = schema->resultset('LocationguideLocationmap')
        ->search( { map_id => $areamapid } );
    
    var page_title => sprintf("Call Number &amp; Collection Locations -  %s : %s", $areamap->building, $areamap->area);
    push @{vars->{body_class}}, 'locationmaps';
      
    template 'locationmaps', {
      locationmaps => $locationmaps,
    }
  };
  
  # matches /locationmaps/buildingname
  get '/:building[Str]' => sub {
    my $building = route_parameters->get('building');
    my $locationmaps = schema->resultset('LocationguideLocationmap')->search(
      { 'map.building' => lc($building) },
      {
        'join'    => 'map',
        'order_by' => { -asc => 'callno_priority' }
      },
    );

    var page_title => sprintf("%s Call Number &amp; Collection Locations", $building);

    push @{vars->{body_class}}, 'locationmaps';

    template 'locationmaps', {
      locationmaps => $locationmaps,
      building => $building,
    }    
  };
  
};

prefix '/locationmap' => sub {
  hook before => sub {
    return unless request->path =~ /^\/locationmap\//;
    # var last_page => request->header('HTTP_REFERER');
  };

  get '/new' => sub {
    my $resultset = schema->resultset('LocationguideLocationmap');
    my $locationmap = $resultset->new_result( { callno_priority => 0 } );

    my $page_title = 'New Location (Call Number and/or Collection)';
    my $building = query_parameters->get('building');
    if ($building) {
      $page_title .= ' - ' . $building;
    }
    var page_title => $page_title;
    push @{vars->{body_class}}, 'locationmap';
    
    template 'locationmap', {
      locationmap => $locationmap,
      split_locationmap => undef,
    };
  };

  post '/new' => sub {
    my $form = body_parameters->as_hashref_multi;
  
    # assume the first element is the original Location Map
    my $form_data = {
      id => undef,
      callno_type => '',
      callno_range => $form->{callno_range}->[0],
      callno_start => $form->{callno_start}->[0],
      callno_end => $form->{callno_end}->[0],
      callno_priority => $form->{callno_priority}->[0],
      collection_group_id => ($form->{collection_group_id}->[0] || undef),
      external_link_id => ($form->{external_link_id}->[0] || undef),
      map_id => ($form->{map_id}->[0] || undef),
    };
    
    debug Dumper $form_data;
    
    my $locationmap = schema->resultset('LocationguideLocationmap')
      ->find_or_create( $form_data, { key => 'primary' } );
    
    send_error( "Unable to create a new Location Map", 500 ) if !$locationmap;
    
    deferred success => "Successfully created new Location Map";
    
    # return to the edit form
    redirect '/locationmap/' . $locationmap->id;

  };

  get '/:id' => sub {
    var app_section => 'locationmaps';
    var app_section_title => 'Location Maps';
    push @{vars->{body_class}}, 'locationmap';
	  my $id = route_parameters->get('id');

	  my $locationmap = schema->resultset('LocationguideLocationmap')->find({ id => $id });

	  my $log_object_name = "Location Map (" . $locationmap->callno_range . "): ";
	  $log_object_name .= $locationmap->map->building . " / " . $locationmap->map if $locationmap->map;
	  $log_object_name .= $locationmap->external_link->title if $locationmap->external_link;

    my $other_locmap;
    if (query_parameters->get('start-callno-split')) {
      # create new location map record
      # clone values from the existing
      # display both in UI

      $other_locmap = schema->resultset('LocationguideLocationmap')->new_result( {
        callno_range => '',
        callno_start => '',
        callno_end => $locationmap->callno_end,
        collection_group_id => $locationmap->collection_group_id,
        map_id => $locationmap->map_id,
        external_link_id => $locationmap->external_link_id,
        callno_priority => $locationmap->callno_priority,
        callno_type => '',
      });

      # clear the "call no end" field since it is set 
      # in the split record (above).
      $locationmap->set_column('callno_end' => '');
      var disable_delete => 1;
    }
      
    # determine the page title
    my $ptitle = "Call Number / Collection Location";
    $ptitle = sprintf("Call Number Range: %s (%s: %s)", $locationmap->callno_range, $locationmap->collection_group->sublibrary->label, $locationmap->collection_group->description) 
      if $locationmap->collection_group && $locationmap->callno_range;
      
    $ptitle = "Call Number Range: " . $locationmap->callno_range 
      if $locationmap->callno_range && !$locationmap->collection_group;
      
    $ptitle = "Collection: " . $locationmap->collection_group->sublibrary->label . ": " . $locationmap->collection_group->description 
      if $locationmap->collection_group && !$locationmap->callno_range;
      
    var page_title => $ptitle;

	  template 'locationmap', {
		  locationmap => $locationmap,
		  split_locationmap => $other_locmap,
	  }
  };

  post '/:id' => sub {
    var app_section => 'locationmaps';
    var app_section_title => 'Location Maps';
    my $form = body_parameters->as_hashref_multi;
  
    # assume the first element is the original Location Map
    my $locationmap = {
      id => $form->{id}->[0],
      callno_type => '',
      callno_range => $form->{callno_range}->[0],
      callno_start => $form->{callno_start}->[0],
      callno_end => $form->{callno_end}->[0],
      callno_priority => $form->{callno_priority}->[0],
      collection_group_id => ($form->{collection_group_id}->[0] || undef),
      external_link_id => ($form->{external_link_id}->[0] || undef),
      map_id => ($form->{map_id}->[0] || undef),
    };
    $locationmap = schema->resultset("LocationguideLocationmap")->update_or_new( $locationmap );
    
    my $other_locmap;
    if (query_parameters->get('start-callno-split')) {
      # create new location map record
      # clone values from the existing
      # display both in UI
      $other_locmap = {
        id => undef,
        callno_type => '',
        callno_range => $form->{callno_range}->[1],
        callno_start => $form->{callno_start}->[1],
        callno_end => $form->{callno_end}->[1],
        callno_priority => $form->{callno_priority}->[1],
        collection_group_id => ($form->{collection_group_id}->[1] || undef),
        external_link_id => ($form->{external_link_id}->[1] || undef),
        map_id => ($form->{map_id}->[1] || undef),
      };
      $other_locmap = schema->resultset("LocationguideLocationmap")->find_or_create( $other_locmap );
    }

    deferred success => "Your changes were saved.";

	  template 'locationmap', {
		  locationmap => $locationmap,
		  split_locationmap => $other_locmap,
	  }
    
  };

  post '/copy/:lmid' => sub {
	  my $locmap_id = route_parameters->get('lmid');
	  my $lmrs = schema
		  ->resultset('LocationguideLocationmap')
		  ->search( { id => $locmap_id } );
	  my $origlm = $lmrs->first();
	  my $copylm = $lmrs->create( {
			  collection_group_id => $origlm->get_column('collection_group_id'),
			  map_id => $origlm->get_column('map_id'),
			  callno_range => $origlm->get_column('callno_range'),
			  callno_type => $origlm->get_column('callno_type'),
			  callno_start => $origlm->get_column('callno_start'),
			  callno_end => $origlm->get_column('callno_end'),
			  external_link_id => $origlm->get_column('external_link_id'),
		  });
  };

  get '/:lmid/delete' => sub {
    my $locmap_id = route_parameters->get('lmid');
    my $locationmap = schema->resultset('LocationguideLocationmap')->find($locmap_id);

    my $redirect_url = '/locationmaps';
    if ($locationmap->map) {
      $redirect_url .= '/' . $locationmap->map->id;
    }
    $locationmap->delete();

    deferred success => "The location map was removed.";
    redirect $redirect_url;
  };

};

__END__

Examples of using WHERE clauses in DBIx::Class::ResultSet:

    # DEPRECATED - remains here for examples of using WHERE clauses in 
    # DBIx::Class::ResultSet
    #my $crit = [
    #  { map_id => { '!=', undef } },
    #  { external_link_id => { '!=', undef } }
    #];
    
    # Also DEPRECATED, kept for example
    # my $crit = { callno_range => { '!=', undef }, callno_range => { '!=', '' } };
