package locguide::AreaMap;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'locguide';
use Dancer2::Plugin::LocGuideCommonData;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Deferred;

# uncomment this when AJAX processing is needed
#use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;
use File::Copy;

prefix '/areamap' => sub {
  get '/new' => sub {
    my $resultset = schema->resultset('LocationguideAreamap');
    my $areamap = $resultset->new_result( {
      building => setting('ui')->{areamaps}->{default_building},
    });
    
    var page_title => 'New Floor / Area Map';
    
    template 'areamap', {
      areamap => $areamap,
      a => $areamap,
    };
  };
  
  post '/new' => sub {
    send_error "Feature not implemented yet", 500;
	  my $uploads = request->uploads;
	  debug( Dumper( $uploads ) );
	  debug( Dumper( config->{media_path} ) );

	  # exit if we don't have a media path
	  if ( !defined( config->{media_path} ) ) {
		  return 0;
	  }

	  my $areamap_image = $uploads->{areamap_image};
	  if (defined( $areamap_image )) {
		  debug( Dumper( sprintf( "%s/%s", config->{areamap_root}, $areamap_image->filename ) ) );
		  $areamap_image->copy_to( sprintf( "%s/%s", config->{areamap_root}, $areamap_image->filename ) ) or carp "could not save file\n";
	  }
	  
    my $data = body_parameters->as_hashref_mixed;
    
    1;
  };
  
  get '/delete/:id' => sub {
    my $areamap = schema->resultset('LocationguideAreamap')->find( { id => var('id') } );
    
    send_error sprintf("Unable to find Area Map record (id: %s)", var('id')), 404
      unless $areamap;
    
    send_error sprintf("Unable to delete Area Map (id: %s)", var('id')), 500
      if !$areamap->delete;
      
    deferred success => "Area Map removed successfully";
    
    redirect '/areamaps';
  };
  
  get '/:id' => sub {
		my $id = route_parameters->get('id');
		my $areamap = schema->resultset('LocationguideAreamap')->find({ id => $id });

		var page_title => sprintf('Area Map: %s - %s', $areamap->building, $areamap->area);

    debug Dumper $areamap->get_columns;

		template 'areamap', {
			areamap => $areamap,
			a => $areamap,
		};
  };
  
  post '/:id' => sub {
    my $id = route_parameters->get('id');
    my $form = body_parameters->as_hashref_mixed;
    $form->{id} = $id;

    my $areamap = schema->resultset('LocationguideAreamap')->find({id => $id});
    send_error sprintf("[post /areamap/:id] unable to find area map record (id: %s", $id), 500
      unless $areamap;

    # retrieve the file from the form post
    my $upload_data = upload("areamap_image");
    if ($upload_data) {
      my $dest_filename = config->{areamap_root} . "/" . $upload_data->{filename};
      debug sprintf("media_path => %s", config->{areamap_root});

      if (!move($upload_data->{tempname}, $dest_filename)) {
        deferred error => "Unable to move the uploaded image: $!";
      } else {
        # grant R/W permissions to the new file
        chmod 0666, $dest_filename;

        $form->{image} = config->{areamap_path_relative} . "/" . $upload_data->{filename};
      }
    }
    debug Dumper $form;

    # Don't actually remove the image from the filesystem
    # just clear the "image" field
    if ($form->{areamap_image_removed}) {
      $form->{image} = "";
    }

    # then delete the 'areamap_image_removed' key from the form
    delete $form->{areamap_image_removed};

    $areamap->update( $form );
    deferred success => "Changes saved.";

    redirect '/areamap/' . $id;
  };
};

prefix '/areamaps' => sub {

  # This route references the following 'vars' created in 'hook before':
  # - var('page')
  # - var('rows')
  get '' => sub {

    my $areamaps = schema->resultset('LocationguideAreamap')
      ->search(undef, { 
            page => var('page'),
            rows => var('rows'),
            order_by => { -asc => [qw/building area/] },
        });
    my $pager = $areamaps->pager;
    
    var page_title => 'Area Maps';
    push @{vars->{body_class}}, 'areamaps';

    template 'areamaps2', {
      pager => $pager,
      areamaps => $areamaps,
    };
  };
};

# DEPRECATED
prefix '/areamaps2' => sub {

  # This route references the following 'vars' created in 'hook before':
  # - var('page')
  # - var('rows')
  get '' => sub {

    my $areamaps = schema->resultset('LocationguideAreamap')
      ->search(undef, { 
            order_by => { -asc => [qw/building area/] },
        });
    
    var page_title => 'Area Maps';

    template 'areamaps2', {
      areamaps => $areamaps,
    };
  };
};

1;

__END__
get '/areamap/new' => sub {
	my $buildings = dul_areamap_buildings();

	if (request->is_ajax) {
		content_type 'application/json';
		return to_json( { objectlabel => 'Area Map', buildings => $buildings } );
	}
};

post '/areamap/new' => sub {
	my $uploads = request->uploads;
	debug( Dumper( $uploads ) );
	debug( Dumper( config->{media_path} ) );

	# exit if we don't have a media path
	if ( !defined( config->{media_path} ) ) {
		return 0;
	}

	my $areamap_image = $uploads->{areamap_image};
	if (defined( $areamap_image )) {
		debug( Dumper( sprintf( "%s/%s", config->{media_path}, $areamap_image->filename ) ) );
		$areamap_image->copy_to( sprintf( "%s/%s", config->{media_path}, $areamap_image->filename ) ) or carp "could not save file\n";
	}

	#my $areamap_image_path = config->{locguide_media_prefix} . $areamap_image->filename;
	#my $sql = "
	#	INSERT INTO locationguide_areamap 
	#	VALUES ('', ?, ?, ?)
	#	";
	#my $sth = database->prepare();
	#$sth->execute params->{areamap_building}, params->{areamap_area}, $areamap_image_path or die database->errstr;

	session areamap_building => params->{areamap_building};
	session areamap_area => params->{areamap_area};
	session show_success_alert => 1;
	session success_alert_message => 'Your map has been uploaded';
	session success_alert_title => 'Success!';
	redirect '/';
};

# 
sub _areamap_for_id {
	my $map_id = shift;
	my $areamap_rs = schema->resultset("LocationguideAreamap")->search(
		{ id => $map_id }
	);
		#{ '+columns' => [ { 'areamap_area' => 'area', 'areamap_building' => 'building', 'areamap_image' => 'image' } ] }
	my $areamap = $areamap_rs->first;
	$areamap;
}

get '/areamap/:id' => sub {
	my $id = route_parameters->get('id');
	my $areamap = _areamap_for_id( $id );

	my $buildings;
	$buildings = vars->{buildings} || ();

	my $accept = request->header( 'accept' );
	if ($accept =~ /json/) {
		content_type 'application/json';
		return to_json( { 
			display => $areamap->{_column_data},
			buildings => $buildings,
		} );
	}
	template 'areamap', { areamap => $areamap->{_column_data} };	
};

get '/areamaps' => sub {
	my $sql = "SELECT * FROM locationguide_areamap ORDER BY building, area";
	my $sth = database->prepare( $sql );
	$sth->execute;
	my $areamaps = $sth->fetchall_arrayref({});

	my $extrajs = [
		{ src => request->uri_base . '/javascripts/dashboard.js' },
	];

	my $extracss = [
		{ href => request->uri_base . '/css/dashboard.css', media => 'screen' },
	];

	template 'areamaps', {
		areamaps => $areamaps,
		extrajs => $extrajs,
		extracss => $extracss,
		activepage => 'areamaps'
	};
};

