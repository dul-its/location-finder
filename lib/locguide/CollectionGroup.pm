package locguide::CollectionGroup;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'locguide';
use Dancer2::Plugin::LocGuideCommonData;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Deferred;

# uncomment this when AJAX processing is needed
#use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;

prefix '/collectiongroup' => sub {
  get '/new' => sub {
    var app_section => 'collectiongroups';
    var app_section_title => 'Collection Groups';
    my $collectiongroup = schema->resultset('LocationguideCollectiongroup')->new_result( {} );
    send_error "Unable to prepare new collection group record for entry!", 500
      unless $collectiongroup;

    var page_title => 'New Collection Group';
    
    template 'collectiongroup', {
      collectiongroup => $collectiongroup,
      'c' => $collectiongroup,
    };
  };
  
  post '/new' => sub {
    my $form = body_parameters->as_hashref;
    $form->{id} = undef;

    my $collectiongroup = schema->resultset('LocationguideCollectiongroup')
      ->find_or_create( $form, { key => 'primary' } );

    send_error( "Unable to create a new Collection Group",500 ) if !$collectiongroup;
    deferred success => "Collection Group created successfully";

    # return to the edit form
    redirect '/collectiongroup/' . $collectiongroup->id;
  };
   
  get '/:id/delete' => sub {
    my $collectiongroup = schema->resultset('LocationguideCollectiongroup')->find( { id => var('id') } );
    send_error sprintf("Unable to find Collection Group record (id: %s)", var('id')), 404
      unless $collectiongroup;
    
    send_error sprintf("Unable to delete Collection Group (id: %s)", var('id')), 500
      if !$collectiongroup->delete;
      
    deferred success => "Collection Group removed successfully";
    
    redirect '/collectiongroups';
  };

  get '/:id/duplicate' => sub {
    my $original = schema->resultset('LocationguideCollectiongroup')->find( { id => var('id') } );
    send_error sprintf("Unable to locate original Collection Group record (id: %s)", var('id')), 500
      unless $original;

    my $copy = $original->copy( { description => sprintf("Copy of %s", $original->description) } );
    if (!$copy) {
      deferred error => "Unable to duplicate the collection group.";
      redirect '/collectiongroups';
    }

    deferred success => "Collection Group duplicated successfully.";
    redirect '/collectiongroup/' . $copy->id;
  };
  
  get '/:id' => sub {
    var app_section => 'collectiongroups';
    var app_section_title => 'Collection Groups';
    my $collectiongroup = schema->resultset('LocationguideCollectiongroup')
      ->find({ id => var 'id' });

    #my $log_object_name = "Collection Group: " . $collectiongroup->description;
    #my $auth_user = schema->resultset('AuthUser')->find( { username => session('user')->{username} } );
    #$auth_user->related_resultset('admin_logs')->create({
    #	action_time 		=> var('now_stamp'),
    #	user_id					=> $auth_user->id,
    #	content_type_id => 301,
    #	object_id				=> var('id'),
    #	object_repr			=> $log_object_name,
    # create_message  => '',
    #});
    
	  if ( !$collectiongroup ) {
		  send_error "Unable to locate collection group for (" . var('id') . ")", 404;
	  }
		var page_title => sprintf('Collection Group: %s', ($collectiongroup->description || 'No Title'));
	  
	  template 'collectiongroup', {
		  collectiongroup => $collectiongroup,
		  'c' => $collectiongroup,
	  };
  };
  
  post '/:id' => sub {
	  my $form = body_parameters->as_hashref_mixed;
	  
    my $collectiongroup = schema->resultset('LocationguideCollectiongroup')->find( { id => var 'id' } );
    if ($collectiongroup->update( $form ) ) {
      # good
      session success_alert_message => "Your changes were saved.";
    } else {
      # TODO add validation to Collection Group result class
      #to_dumper $collectiongroup->result_errors;
    }
    
    # return to the edit form
    forward '/collectiongroup/' . route_parameters->get('id'), undef, { method => 'GET' };
  };
};

prefix '/collectiongroups' => sub {
  get '' => sub {
    debug "in /collectiongroups";

    # get filter values (if any)
    my $filter_sublibrary = query_parameters->get('sublibrary') || query_parameters->get('s') || undef;
    my $filter_description = query_parameters->get('description') || query_parameters->get('d') || '';

    my $filter = {};
    $filter->{description} = {'!=', ''} if (!$filter_sublibrary && !$filter_description);
    $filter->{description} = {'like', sprintf("%s\%", $filter_description) } if $filter_description;
    $filter->{sublibrary_id} = $filter_sublibrary if $filter_sublibrary;
    var search_filter => $filter;

	  my $collectiongroups = schema->resultset('LocationguideCollectiongroup')->search(
		  $filter,
		  {
        page => var('page'),
        rows => var('rows'),
        order_by => var('page_settings')->{order_by},
		  }
	  );
    my $pager = $collectiongroups->pager;
  
	  template 'collectiongroups', {
		  collectiongroups => $collectiongroups,
		  pager => $pager,
		  activepage => 'collectiongroups',
	  };
  };
};

1;

__END__

