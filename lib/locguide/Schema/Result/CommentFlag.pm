use utf8;
package locguide::Schema::Result::CommentFlag;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::CommentFlag

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<comment_flags>

=cut

__PACKAGE__->table("comment_flags");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 comment_id

  data_type: 'integer'
  is_nullable: 0

=head2 flag

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 flag_date

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "comment_id",
  { data_type => "integer", is_nullable => 0 },
  "flag",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "flag_date",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<user_id>

=over 4

=item * L</user_id>

=item * L</comment_id>

=item * L</flag>

=back

=cut

__PACKAGE__->add_unique_constraint("user_id", ["user_id", "comment_id", "flag"]);

=head1 RELATIONS

=head2 user

Type: belongs_to

Related object: L<locguide::Schema::Result::AuthUser>

=cut

__PACKAGE__->belongs_to(
  "user",
  "locguide::Schema::Result::AuthUser",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-02-13 15:39:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:AZAjxQNW4hx0xtdVVPBC5g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
