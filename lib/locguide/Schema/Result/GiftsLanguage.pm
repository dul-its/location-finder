use utf8;
package locguide::Schema::Result::GiftsLanguage;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::GiftsLanguage

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<gifts_language>

=cut

__PACKAGE__->table("gifts_language");

=head1 ACCESSORS

=head2 code

  data_type: 'varchar'
  is_nullable: 0
  size: 3

=head2 value

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 in_pick_list

  data_type: 'tinyint'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "code",
  { data_type => "varchar", is_nullable => 0, size => 3 },
  "value",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "in_pick_list",
  { data_type => "tinyint", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</code>

=back

=cut

__PACKAGE__->set_primary_key("code");

=head1 UNIQUE CONSTRAINTS

=head2 C<value>

=over 4

=item * L</value>

=back

=cut

__PACKAGE__->add_unique_constraint("value", ["value"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4SN87KzoMpJlIMUzZdQRXA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
