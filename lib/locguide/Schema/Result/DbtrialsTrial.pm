use utf8;
package locguide::Schema::Result::DbtrialsTrial;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::DbtrialsTrial

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<dbtrials_trial>

=cut

__PACKAGE__->table("dbtrials_trial");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 url

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 provider

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 consortium

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 username

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 password

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 description

  data_type: 'longtext'
  is_nullable: 0

=head2 status

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 end_date

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 one_time_cost

  data_type: 'decimal'
  is_nullable: 1
  size: [9,2]

=head2 annual_cost

  data_type: 'decimal'
  is_nullable: 1
  size: [9,2]

=head2 annual_access_fee

  data_type: 'decimal'
  is_nullable: 1
  size: [9,2]

=head2 requested_by

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 notes

  data_type: 'longtext'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "url",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "provider",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "consortium",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "username",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "password",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "description",
  { data_type => "longtext", is_nullable => 0 },
  "status",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "end_date",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 1 },
  "one_time_cost",
  { data_type => "decimal", is_nullable => 1, size => [9, 2] },
  "annual_cost",
  { data_type => "decimal", is_nullable => 1, size => [9, 2] },
  "annual_access_fee",
  { data_type => "decimal", is_nullable => 1, size => [9, 2] },
  "requested_by",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "notes",
  { data_type => "longtext", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<title>

=over 4

=item * L</title>

=back

=cut

__PACKAGE__->add_unique_constraint("title", ["title"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:cD64Klnw/EbmTOnLtRjH3g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
