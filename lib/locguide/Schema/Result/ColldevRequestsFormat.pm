use utf8;
package locguide::Schema::Result::ColldevRequestsFormat;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::ColldevRequestsFormat

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<colldev_requests_format>

=cut

__PACKAGE__->table("colldev_requests_format");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 value

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "value",
  { data_type => "varchar", is_nullable => 0, size => 40 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<value>

=over 4

=item * L</value>

=back

=cut

__PACKAGE__->add_unique_constraint("value", ["value"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Io999MpZ1EmOIopn6G05+Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
