use utf8;
package locguide::Schema::Result::ColldevRequestsTitlerequest;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::ColldevRequestsTitlerequest

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<colldev_requests_titlerequest>

=cut

__PACKAGE__->table("colldev_requests_titlerequest");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 requester_name

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 requester_netid

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 requester_uniqueid

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 requester_affiliation

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 requester_dept

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 requester_address

  data_type: 'longtext'
  is_nullable: 0

=head2 requester_phone

  data_type: 'varchar'
  is_nullable: 0
  size: 80

=head2 requester_email

  data_type: 'varchar'
  is_nullable: 0
  size: 75

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 author

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 edition

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 format_id

  data_type: 'integer'
  is_nullable: 0

=head2 other_format

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 volumes

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 isbn

  data_type: 'varchar'
  is_nullable: 0
  size: 13

=head2 issn

  data_type: 'varchar'
  is_nullable: 0
  size: 8

=head2 publisher

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 pub_place

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 pub_year

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 info_source

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 additional_info

  data_type: 'longtext'
  is_nullable: 0

=head2 notes

  data_type: 'longtext'
  is_nullable: 0

=head2 librarian_id

  data_type: 'integer'
  is_nullable: 1

=head2 status_id

  data_type: 'integer'
  is_nullable: 0

=head2 created_on

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "requester_name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "requester_netid",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "requester_uniqueid",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "requester_affiliation",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "requester_dept",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "requester_address",
  { data_type => "longtext", is_nullable => 0 },
  "requester_phone",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "requester_email",
  { data_type => "varchar", is_nullable => 0, size => 75 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "author",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "edition",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "format_id",
  { data_type => "integer", is_nullable => 0 },
  "other_format",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "volumes",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "isbn",
  { data_type => "varchar", is_nullable => 0, size => 13 },
  "issn",
  { data_type => "varchar", is_nullable => 0, size => 8 },
  "publisher",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "pub_place",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "pub_year",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "info_source",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "additional_info",
  { data_type => "longtext", is_nullable => 0 },
  "notes",
  { data_type => "longtext", is_nullable => 0 },
  "librarian_id",
  { data_type => "integer", is_nullable => 1 },
  "status_id",
  { data_type => "integer", is_nullable => 0 },
  "created_on",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:nq2nK0NFpKAwJ4FWB+AHVw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
