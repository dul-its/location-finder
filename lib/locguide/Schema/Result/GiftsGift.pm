use utf8;
package locguide::Schema::Result::GiftsGift;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::GiftsGift

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<gifts_gift>

=cut

__PACKAGE__->table("gifts_gift");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 donor_id

  data_type: 'integer'
  is_nullable: 0

=head2 date_received

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 date_processed

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 date_receipt_sent

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 receipt_sent_by

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 gift_plate

  data_type: 'tinyint'
  is_nullable: 0

=head2 language_id

  data_type: 'varchar'
  is_nullable: 1
  size: 3

=head2 appraised

  data_type: 'tinyint'
  is_nullable: 0

=head2 condition

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 estimated_value

  data_type: 'decimal'
  is_nullable: 1
  size: [9,2]

=head2 rbmscl

  data_type: 'tinyint'
  is_nullable: 0

=head2 subscription

  data_type: 'tinyint'
  is_nullable: 0

=head2 description

  data_type: 'longtext'
  is_nullable: 0

=head2 notes

  data_type: 'longtext'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "donor_id",
  { data_type => "integer", is_nullable => 0 },
  "date_received",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 1 },
  "date_processed",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 1 },
  "date_receipt_sent",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 1 },
  "receipt_sent_by",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "gift_plate",
  { data_type => "tinyint", is_nullable => 0 },
  "language_id",
  { data_type => "varchar", is_nullable => 1, size => 3 },
  "appraised",
  { data_type => "tinyint", is_nullable => 0 },
  "condition",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "estimated_value",
  { data_type => "decimal", is_nullable => 1, size => [9, 2] },
  "rbmscl",
  { data_type => "tinyint", is_nullable => 0 },
  "subscription",
  { data_type => "tinyint", is_nullable => 0 },
  "description",
  { data_type => "longtext", is_nullable => 0 },
  "notes",
  { data_type => "longtext", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:MXoxgd6bAbdrf6nOFiaYWg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
