use utf8;
package locguide::Schema::Result::AdminLog;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::AdminLog

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<admin_log>

=cut

__PACKAGE__->table("admin_log");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 action_time

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 content_type_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 object_id

  data_type: 'longtext'
  is_nullable: 1

=head2 object_repr

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 action_flag

  data_type: 'smallint'
  extra: {unsigned => 1}
  is_nullable: 0

=head2 change_message

  data_type: 'longtext'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "action_time",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "content_type_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "object_id",
  { data_type => "longtext", is_nullable => 1 },
  "object_repr",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "action_flag",
  { data_type => "smallint", extra => { unsigned => 1 }, is_nullable => 0 },
  "change_message",
  { data_type => "longtext", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 content_type

Type: belongs_to

Related object: L<locguide::Schema::Result::ContentType>

=cut

__PACKAGE__->belongs_to(
  "content_type",
  "locguide::Schema::Result::ContentType",
  { id => "content_type_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 user

Type: belongs_to

Related object: L<locguide::Schema::Result::AuthUser>

=cut

__PACKAGE__->belongs_to(
  "user",
  "locguide::Schema::Result::AuthUser",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-02-13 15:39:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:wOTEUHnOdppfmznxQ8m3Rg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
