use utf8;
package locguide::Schema::Result::AppdocsApplication;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::AppdocsApplication

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<appdocs_application>

=cut

__PACKAGE__->table("appdocs_application");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 description

  data_type: 'longtext'
  is_nullable: 0

=head2 status

  data_type: 'varchar'
  is_nullable: 0
  size: 60

=head2 language

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 framework

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 db_type

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 db_name

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 db_user

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 vcs_url

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 func_owner

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 developer

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 admin_url

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 public_url

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 doc_url

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 install_path

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 log

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 notes

  data_type: 'longtext'
  is_nullable: 0

=head2 updated_on

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "description",
  { data_type => "longtext", is_nullable => 0 },
  "status",
  { data_type => "varchar", is_nullable => 0, size => 60 },
  "language",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "framework",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "db_type",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "db_name",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "db_user",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "vcs_url",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "func_owner",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "developer",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "admin_url",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "public_url",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "doc_url",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "install_path",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "log",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "notes",
  { data_type => "longtext", is_nullable => 0 },
  "updated_on",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<name>

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("name", ["name"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:OjPpAtWXt40gl9Z+ygKKng


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
