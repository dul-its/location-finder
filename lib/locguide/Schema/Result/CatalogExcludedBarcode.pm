use utf8;
package locguide::Schema::Result::CatalogExcludedBarcode;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::CatalogExcludedBarcode

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<catalog_excluded_barcodes>

=cut

__PACKAGE__->table("catalog_excluded_barcodes");

=head1 ACCESSORS

=head2 ebid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 barcode

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 ignore

  data_type: 'tinyint'
  default_value: 0
  extra: {unsigned => 1}
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "ebid",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "barcode",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "ignore",
  {
    data_type => "tinyint",
    default_value => 0,
    extra => { unsigned => 1 },
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</ebid>

=back

=cut

__PACKAGE__->set_primary_key("ebid");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:xKC103tN9pVvb04wt1ashA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
