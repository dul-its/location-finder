use utf8;
package locguide::Schema::Result::LocationguideGenericmessage;

__PACKAGE__->ensure_class_loaded('DBIx::Class::Storage::DBI::MariaDB');

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::LocationguideGenericmessage

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<locationguide_genericmessage>

=cut

__PACKAGE__->table("locationguide_genericmessage");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 label

  data_type: 'varchar'
  is_nullable: 0
  size: 60

=head2 content

  data_type: 'longtext'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "label",
  { data_type => "varchar", is_nullable => 0, size => 60 },
  "content",
  { data_type => "longtext", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<label>

=over 4

=item * L</label>

=back

=cut

__PACKAGE__->add_unique_constraint("label", ["label"]);

=head1 RELATIONS

=head2 locationguide_externallinks

Type: has_many

Related object: L<locguide::Schema::Result::LocationguideExternallink>

=cut

__PACKAGE__->has_many(
  "locationguide_externallinks",
  "locguide::Schema::Result::LocationguideExternallink",
  { "foreign.generic_msg_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-02-11 16:50:02
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:CrXtsTAdpjIDwWoCLGNVOQ


__PACKAGE__->load_components(qw/ Result::Validation /);

sub _validate {
  my $self = shift;
  $self->validate_not_empty('label');
  $self->validate_not_empty('content');
}
1;
