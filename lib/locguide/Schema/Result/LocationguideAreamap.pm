use utf8;
package locguide::Schema::Result::LocationguideAreamap;

__PACKAGE__->ensure_class_loaded('DBIx::Class::Storage::DBI::MariaDB');

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::LocationguideAreamap

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<locationguide_areamap>

=cut

__PACKAGE__->table("locationguide_areamap");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 building

  data_type: 'varchar'
  is_nullable: 0
  size: 10

=head2 area

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 image

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "building",
  { data_type => "varchar", is_nullable => 0, size => 10 },
  "area",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "image",
  { data_type => "varchar", is_nullable => 0, size => 100 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 locationguide_collections

Type: has_many

Related object: L<locguide::Schema::Result::LocationguideCollection>

=cut

__PACKAGE__->has_many(
  "locationguide_collections",
  "locguide::Schema::Result::LocationguideCollection",
  { "foreign.map_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 locationguide_locationmaps

Type: has_many

Related object: L<locguide::Schema::Result::LocationguideLocationmap>

=cut

__PACKAGE__->has_many(
  "locationguide_locationmaps",
  "locguide::Schema::Result::LocationguideLocationmap",
  { "foreign.map_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-03-16 15:20:05
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Ft5muqZy5ZaBChriFnBCEA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
