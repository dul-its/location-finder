use utf8;
package locguide::Schema::Result::AuthorityListsAuthoritylist;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::AuthorityListsAuthoritylist

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<authority_lists_authoritylist>

=cut

__PACKAGE__->table("authority_lists_authoritylist");

=head1 ACCESSORS

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 group_id

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 description

  data_type: 'longtext'
  is_nullable: 0

=head2 use_values_for_display

  data_type: 'tinyint'
  is_nullable: 0

=head2 updateable

  data_type: 'tinyint'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "title",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "group_id",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "description",
  { data_type => "longtext", is_nullable => 0 },
  "use_values_for_display",
  { data_type => "tinyint", is_nullable => 0 },
  "updateable",
  { data_type => "tinyint", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</title>

=back

=cut

__PACKAGE__->set_primary_key("title");

=head1 UNIQUE CONSTRAINTS

=head2 C<title>

=over 4

=item * L</title>

=item * L</group_id>

=back

=cut

__PACKAGE__->add_unique_constraint("title", ["title", "group_id"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:mkBGWmP0zlx5NZtxIntXDA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
