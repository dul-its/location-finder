use utf8;
package locguide::Schema::Result::LocationguideSublibrary;

__PACKAGE__->ensure_class_loaded('DBIx::Class::Storage::DBI::MariaDB');

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::LocationguideSublibrary

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<locationguide_sublibrary>

=cut

__PACKAGE__->table("locationguide_sublibrary");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 code

  data_type: 'varchar'
  is_nullable: 0
  size: 8

=head2 label

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 collgroup_choice

  data_type: 'tinyint'
  is_nullable: 0

=head2 default_locmap_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "code",
  { data_type => "varchar", is_nullable => 0, size => 8 },
  "label",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "collgroup_choice",
  { data_type => "tinyint", is_nullable => 0 },
  "default_locmap_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<code>

=over 4

=item * L</code>

=back

=cut

__PACKAGE__->add_unique_constraint("code", ["code"]);

=head2 C<default_locmap_id>

=over 4

=item * L</default_locmap_id>

=back

=cut

__PACKAGE__->add_unique_constraint("default_locmap_id", ["default_locmap_id"]);

=head1 RELATIONS

=head2 default_locmap

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideLocationmap>

=cut

__PACKAGE__->belongs_to(
  "default_locmap",
  "locguide::Schema::Result::LocationguideLocationmap",
  { id => "default_locmap_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 locationguide_collectiongroups

Type: has_many

Related object: L<locguide::Schema::Result::LocationguideCollectiongroup>

=cut

__PACKAGE__->has_many(
  "locationguide_collectiongroups",
  "locguide::Schema::Result::LocationguideCollectiongroup",
  { "foreign.sublibrary_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 locationguide_collections

Type: has_many

Related object: L<locguide::Schema::Result::LocationguideCollection>

=cut

__PACKAGE__->has_many(
  "locationguide_collections",
  "locguide::Schema::Result::LocationguideCollection",
  { "foreign.sublibrary_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-02-11 16:55:25
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:W1MvsOUvJ9buJm/6PfoC1g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
