use utf8;
package locguide::Schema::Result::GiftsGiftSubject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::GiftsGiftSubject

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<gifts_gift_subject>

=cut

__PACKAGE__->table("gifts_gift_subject");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 gift_id

  data_type: 'integer'
  is_nullable: 0

=head2 subject_id

  data_type: 'integer'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "gift_id",
  { data_type => "integer", is_nullable => 0 },
  "subject_id",
  { data_type => "integer", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<gift_id>

=over 4

=item * L</gift_id>

=item * L</subject_id>

=back

=cut

__PACKAGE__->add_unique_constraint("gift_id", ["gift_id", "subject_id"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Xwl7kvmPtTObJawutsLErw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
