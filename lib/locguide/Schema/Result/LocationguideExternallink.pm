use utf8;
package locguide::Schema::Result::LocationguideExternallink;

__PACKAGE__->ensure_class_loaded('DBIx::Class::Storage::DBI::MariaDB');

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::LocationguideExternallink

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<locationguide_externallink>

=cut

__PACKAGE__->table("locationguide_externallink");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 url

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 link_text

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 generic_msg_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 content

  data_type: 'longtext'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "url",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "link_text",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "generic_msg_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "content",
  { data_type => "longtext", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 generic_msg

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideGenericmessage>

=cut

__PACKAGE__->belongs_to(
  "generic_msg",
  "locguide::Schema::Result::LocationguideGenericmessage",
  { id => "generic_msg_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 locationguide_collections

Type: has_many

Related object: L<locguide::Schema::Result::LocationguideCollection>

=cut

__PACKAGE__->has_many(
  "locationguide_collections",
  "locguide::Schema::Result::LocationguideCollection",
  { "foreign.external_link_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 locationguide_locationmaps

Type: has_many

Related object: L<locguide::Schema::Result::LocationguideLocationmap>

=cut

__PACKAGE__->has_many(
  "locationguide_locationmaps",
  "locguide::Schema::Result::LocationguideLocationmap",
  { "foreign.external_link_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-03-16 15:20:05
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gIh4dJoFQFYwnhC2gyVN7g


__PACKAGE__->load_components(qw/ Result::Validation /);

sub _validate {
  my $self = shift;
  $self->validate_not_empty('title');
  $self->validate_not_empty('url');
  $self->validate_not_empty('link_text');
  
  if (($self->generic_msg_id eq '') && ($self->content eq '')) {
    $self->add_result_error('message content', 'Generic Message or Custom Content must be present.');
  }
}

sub actual_content {
  my $self = shift;
  if ($self->generic_msg) {
    return $self->generic_msg->content;
  }
  return $self->content;
}
1;
