use utf8;
package locguide::Schema::Result::EasyThumbnailsSource;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::EasyThumbnailsSource

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<easy_thumbnails_source>

=cut

__PACKAGE__->table("easy_thumbnails_source");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 storage_hash

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 modified

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "storage_hash",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "modified",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<storage_hash>

=over 4

=item * L</storage_hash>

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("storage_hash", ["storage_hash", "name"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:RXB43EzT2PBjTGksJ7HkeA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
