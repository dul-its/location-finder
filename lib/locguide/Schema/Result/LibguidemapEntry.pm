use utf8;
package locguide::Schema::Result::LibguidemapEntry;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::LibguidemapEntry

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<libguidemap_entry>

=cut

__PACKAGE__->table("libguidemap_entry");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 subject_code

  data_type: 'varchar'
  is_nullable: 0
  size: 8

=head2 subject_desc

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 library

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 guide_url

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "subject_code",
  { data_type => "varchar", is_nullable => 0, size => 8 },
  "subject_desc",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "library",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "guide_url",
  { data_type => "varchar", is_nullable => 0, size => 200 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<subject_code>

=over 4

=item * L</subject_code>

=back

=cut

__PACKAGE__->add_unique_constraint("subject_code", ["subject_code"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:V4u5gPNrmVR/UhwJlInHDg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
