use utf8;
package locguide::Schema::Result::GiftsDonor;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::GiftsDonor

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<gifts_donor>

=cut

__PACKAGE__->table("gifts_donor");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 donor_type

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 status

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 category

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 first_name

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 middle_name

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 last_name

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 honors

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 address

  data_type: 'longtext'
  is_nullable: 0

=head2 city

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 state_country

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 postal_code

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 digital_contact

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 notes

  data_type: 'longtext'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "donor_type",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "status",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "category",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "first_name",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "middle_name",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "last_name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "honors",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "address",
  { data_type => "longtext", is_nullable => 0 },
  "city",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "state_country",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "postal_code",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "digital_contact",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "notes",
  { data_type => "longtext", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:nQs72TB4r0DLoOB+9qWVxw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
