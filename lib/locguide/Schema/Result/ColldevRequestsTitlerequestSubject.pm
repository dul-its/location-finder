use utf8;
package locguide::Schema::Result::ColldevRequestsTitlerequestSubject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::ColldevRequestsTitlerequestSubject

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<colldev_requests_titlerequest_subject>

=cut

__PACKAGE__->table("colldev_requests_titlerequest_subject");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 titlerequest_id

  data_type: 'integer'
  is_nullable: 0

=head2 subject_id

  data_type: 'integer'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "titlerequest_id",
  { data_type => "integer", is_nullable => 0 },
  "subject_id",
  { data_type => "integer", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<titlerequest_id>

=over 4

=item * L</titlerequest_id>

=item * L</subject_id>

=back

=cut

__PACKAGE__->add_unique_constraint("titlerequest_id", ["titlerequest_id", "subject_id"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jNgYDYfY0G4l5JE2D8aXww


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
