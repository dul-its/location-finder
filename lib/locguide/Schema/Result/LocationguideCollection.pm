use utf8;
package locguide::Schema::Result::LocationguideCollection;

__PACKAGE__->ensure_class_loaded('DBIx::Class::Storage::DBI::MariaDB');

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::LocationguideCollection

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<locationguide_collection>

=cut

__PACKAGE__->table("locationguide_collection");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 code

  data_type: 'varchar'
  is_nullable: 0
  size: 8

=head2 label

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 sublibrary_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 collection_group_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 map_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 external_link_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "code",
  { data_type => "varchar", is_nullable => 0, size => 8 },
  "label",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "sublibrary_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "collection_group_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "map_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "external_link_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<code>

=over 4

=item * L</code>

=back

=cut

__PACKAGE__->add_unique_constraint("code", ["code"]);

=head1 RELATIONS

=head2 collection_group

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideCollectiongroup>

=cut

__PACKAGE__->belongs_to(
  "collection_group",
  "locguide::Schema::Result::LocationguideCollectiongroup",
  { id => "collection_group_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 external_link

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideExternallink>

=cut

__PACKAGE__->belongs_to(
  "external_link",
  "locguide::Schema::Result::LocationguideExternallink",
  { id => "external_link_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 map

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideAreamap>

=cut

__PACKAGE__->belongs_to(
  "map",
  "locguide::Schema::Result::LocationguideAreamap",
  { id => "map_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 sublibrary

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideSublibrary>

=cut

__PACKAGE__->belongs_to(
  "sublibrary",
  "locguide::Schema::Result::LocationguideSublibrary",
  { id => "sublibrary_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-03-16 15:20:05
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:N3OxyQm50SS+erXxG2H4bA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
