use utf8;
package locguide::Schema::Result::GiftsSubject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::GiftsSubject

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<gifts_subject>

=cut

__PACKAGE__->table("gifts_subject");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 subject

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "subject",
  { data_type => "varchar", is_nullable => 0, size => 255 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<subject>

=over 4

=item * L</subject>

=back

=cut

__PACKAGE__->add_unique_constraint("subject", ["subject"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:n/oSLL1wznVDd7brOu/9Ug


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
