use utf8;
package locguide::Schema::Result::AuthorityListsAuthorityterm;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::AuthorityListsAuthorityterm

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<authority_lists_authorityterm>

=cut

__PACKAGE__->table("authority_lists_authorityterm");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 value

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 display

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 authority_list_id

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 sort_order

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 blacklisted

  data_type: 'tinyint'
  is_nullable: 0

=head2 recommended

  data_type: 'tinyint'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "value",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "display",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "authority_list_id",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "sort_order",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "blacklisted",
  { data_type => "tinyint", is_nullable => 0 },
  "recommended",
  { data_type => "tinyint", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<authority_list_id>

=over 4

=item * L</authority_list_id>

=item * L</value>

=back

=cut

__PACKAGE__->add_unique_constraint("authority_list_id", ["authority_list_id", "value"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:uk59ga6K70aR2zvzjXS3KA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
