use utf8;
package locguide::Schema::Result::ColldevRequestsEmailrecord;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::ColldevRequestsEmailrecord

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<colldev_requests_emailrecord>

=cut

__PACKAGE__->table("colldev_requests_emailrecord");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 title_request_id

  data_type: 'integer'
  is_nullable: 0

=head2 message

  data_type: 'longtext'
  is_nullable: 0

=head2 recorded

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "title_request_id",
  { data_type => "integer", is_nullable => 0 },
  "message",
  { data_type => "longtext", is_nullable => 0 },
  "recorded",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:W7ckclR+NUF431LACictQg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
