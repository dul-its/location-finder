use utf8;
package locguide::Schema::Result::ContentType;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::ContentType

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<content_type>

=cut

__PACKAGE__->table("content_type");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 app_label

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 model

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "app_label",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "model",
  { data_type => "varchar", is_nullable => 0, size => 100 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<app_label>

=over 4

=item * L</app_label>

=item * L</model>

=back

=cut

__PACKAGE__->add_unique_constraint("app_label", ["app_label", "model"]);

=head1 RELATIONS

=head2 admin_logs

Type: has_many

Related object: L<locguide::Schema::Result::AdminLog>

=cut

__PACKAGE__->has_many(
  "admin_logs",
  "locguide::Schema::Result::AdminLog",
  { "foreign.content_type_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 comments

Type: has_many

Related object: L<locguide::Schema::Result::Comment>

=cut

__PACKAGE__->has_many(
  "comments",
  "locguide::Schema::Result::Comment",
  { "foreign.content_type_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 favorites

Type: has_many

Related object: L<locguide::Schema::Result::Favorite>

=cut

__PACKAGE__->has_many(
  "favorites",
  "locguide::Schema::Result::Favorite",
  { "foreign.content_type_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-02-13 15:39:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6ODOBrLntkGN256kK4nlzA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
