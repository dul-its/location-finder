use utf8;
package locguide::Schema::Result::LocationguideCollectiongroup;

__PACKAGE__->ensure_class_loaded('DBIx::Class::Storage::DBI::MariaDB');

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::LocationguideCollectiongroup

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<locationguide_collectiongroup>

=cut

__PACKAGE__->table("locationguide_collectiongroup");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 sublibrary_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 description

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "sublibrary_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "description",
  { data_type => "varchar", is_nullable => 0, size => 255 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 locationguide_collections

Type: has_many

Related object: L<locguide::Schema::Result::LocationguideCollection>

=cut

__PACKAGE__->has_many(
  "locationguide_collections",
  "locguide::Schema::Result::LocationguideCollection",
  { "foreign.collection_group_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 locationguide_locationmaps

Type: has_many

Related object: L<locguide::Schema::Result::LocationguideLocationmap>

=cut

__PACKAGE__->has_many(
  "locationguide_locationmaps",
  "locguide::Schema::Result::LocationguideLocationmap",
  { "foreign.collection_group_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 sublibrary

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideSublibrary>

=cut

__PACKAGE__->belongs_to(
  "sublibrary",
  "locguide::Schema::Result::LocationguideSublibrary",
  { id => "sublibrary_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-02-11 16:55:25
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:uUZJ4ChBLEcLCASsdV4vyQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
