use utf8;
package locguide::Schema::Result::LocationguideLocationmap;

__PACKAGE__->ensure_class_loaded('DBIx::Class::Storage::DBI::MariaDB');

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::LocationguideLocationmap

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<locationguide_locationmap>

=cut

__PACKAGE__->table("locationguide_locationmap");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 collection_group_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 map_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 callno_range

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 callno_type

  data_type: 'varchar'
  is_nullable: 0
  size: 1

=head2 callno_start

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 callno_end

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 external_link_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 map_desc

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 callno_priority

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

Used when collections share a call number range.

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "collection_group_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "map_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "callno_range",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "callno_type",
  { data_type => "varchar", is_nullable => 0, size => 1 },
  "callno_start",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "callno_end",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "external_link_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "map_desc",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "callno_priority",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 collection_group

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideCollectiongroup>

=cut

__PACKAGE__->belongs_to(
  "collection_group",
  "locguide::Schema::Result::LocationguideCollectiongroup",
  { id => "collection_group_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 external_link

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideExternallink>

=cut

__PACKAGE__->belongs_to(
  "external_link",
  "locguide::Schema::Result::LocationguideExternallink",
  { id => "external_link_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 locationguide_sublibrary

Type: might_have

Related object: L<locguide::Schema::Result::LocationguideSublibrary>

=cut

__PACKAGE__->might_have(
  "locationguide_sublibrary",
  "locguide::Schema::Result::LocationguideSublibrary",
  { "foreign.default_locmap_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 map

Type: belongs_to

Related object: L<locguide::Schema::Result::LocationguideAreamap>

=cut

__PACKAGE__->belongs_to(
  "map",
  "locguide::Schema::Result::LocationguideAreamap",
  { id => "map_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-02-11 16:55:25
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PeUxk3vwqfiqsro1pBsNYg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
use Data::Dumper;

sub display_label {
  my $self = shift;
  
  my $label = "";

  if ($self->collection_group && $self->collection_group->description) {
    $label = $self->collection_group->description;
  }
  if ($self->map) {
    $label = sprintf("%s: %s", $self->map->building, $self->map->area);
  }
  $label .= " [" . $self->callno_range . "]" if $self->callno_range;

  if ($self->external_link) {
    $label = $self->external_link->title;
  }
  return $label;
}

sub map_label_or_message {
  my $self = shift;
  my $out = '';

  if ($self->external_link) {
    $out = $self->external_link->title;
  } else {
    # area map
    $out = $self->map->building . ': ' . $self->map->area;
  }
}
1;
