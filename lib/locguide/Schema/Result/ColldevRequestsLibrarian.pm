use utf8;
package locguide::Schema::Result::ColldevRequestsLibrarian;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

locguide::Schema::Result::ColldevRequestsLibrarian

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<colldev_requests_librarian>

=cut

__PACKAGE__->table("colldev_requests_librarian");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 person_id

  data_type: 'integer'
  is_nullable: 1

=head2 initials

  data_type: 'varchar'
  is_nullable: 0
  size: 5

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "person_id",
  { data_type => "integer", is_nullable => 1 },
  "initials",
  { data_type => "varchar", is_nullable => 0, size => 5 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<person_id>

=over 4

=item * L</person_id>

=back

=cut

__PACKAGE__->add_unique_constraint("person_id", ["person_id"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-13 11:19:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:a1cdFr6Cj1A73fg9YrTtiA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
