package locguide::MapInfo;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'locguide';
use Dancer2::Plugin::Ajax;
use Dancer2::Plugin::DBIC;

# uncomment this when AJAX processing is needed
#use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;

# We will have two endoints -- see below:
prefix '/api' => sub {

  # /api/mapinfo
  # Process AJAX POST requests originating from 
  # our "discovery" site (find.library.duke.edu)
  #
  # This endpoint expects a JSON object with three properties:
  # 1) callno
  # 2) collection_code
  # 3) sublibrary
  # (the absence of any of these will trigger an error)
  #
  # The output will be a JSON-formatted data structure (object)
  post '/mapinfo' => sub {
    debug "/api/mapinfo route -- made it here!";
    debug Dumper request->headers;

    my $data;
    $data = decode_json(request->body);
    #if (request->header('content-type') ne 'application/x-www-form-urlencoded') {
    #  # debug "JSON request...";
    #  $data = decode_json(request->body);
    #} else {
    #  debug body_parameters;
    #  $data->{collection_code} = body_parameters->{collection_code};
    #  $data->{callno} = body_parameters->{callno};
    #  $data->{sublibrary} = body_parameters->{sublibrary};
    #}

    #my $data = decode_json(request->body);
    # my $data = from_json(request->body);
    debug Dumper $data;

    # send_error "Invalid parameters", 500 
    #   unless $data->{callno} && $data->{collection_code} && $data->{sublibrary};

    my %return_data = (
        server_name => config->{mapfinder}->{server_name},
        media_path => config->{locguide_mediapath},
    );

    my $o = undef;
    my $no_location_data = 0;

    # First, attempt to locate the location map by inspecting 
    # the collection code, and (optionally) a call number 
    # range.
    if ( $data->{callno} && $data->{collection_code} ) {
      $o = schema->resultset("LocationguideLocationmap")->search(
        [
          -and => [
            'locationguide_collections.code' => $data->{collection_code},
            -or => [
              { callno_range => '' },
              {
                callno_range  => { '!=', '' },
                callno_start  => { '<=', $data->{callno} },
                callno_end    => { '>=', $data->{callno} }
              },
            ],
          ]
        ],
        {
          join => { 'collection_group' => 'locationguide_collections' }
        }
      )->first;
    }

    # flag that we have no data when the above query returns 'undef' and 
    # the calling source (likely the Discovery webapp) doesn't provide 
    # a 'sublibrary' code.
    $no_location_data = !$o && !$data->{sublibrary};

    # If the first query doesn't return anything, try locating
    # a location map based on a sublibrary code.
    $o = schema->resultset("LocationguideLocationmap")->search(
      {
        'locationguide_sublibrary.code' => $data->{sublibrary}
      },
      { 
        join => 'locationguide_sublibrary'
      }
    )->first if !$o && $data->{sublibrary};

    my $areamap = $o->map || undef;
    my $external_link = $o->external_link || undef;

    $no_location_data ||= (!$areamap && !$external_link);

    if ( $no_location_data ) {
      $return_data{no_data} = 1;
      $return_data{error} = 'Unable to location map or external link';
      return to_json( \%return_data );
    }

    if ( $areamap ) {
      debug "[mapinfo] we have an areaamp";
      %return_data = (%return_data, $areamap->get_columns);
      $return_data{theme} = 'area-map';
    }

    if ( !$areamap && $external_link ) {
      $return_data{external_url} = $external_link->url;
      $return_data{extlink_title} = $external_link->title;
      $return_data{extlink_text} = $external_link->link_text;
      $return_data{extlink_content} = $external_link->content;

      debug "[mapinfo] we have a generic message" if defined $external_link->generic_msg;

      if ($external_link->generic_msg) {
        $return_data{generic_msg_content} = $external_link->generic_msg->content;
        $return_data{generic_msg_label} = $external_link->generic_msg->label;
      } else {
        $return_data{generic_msg_content} = $external_link->content;
        $return_data{generic_msg_label} = $external_link->title;
      }
      $return_data{generic_msg_content} = $external_link->generic_msg->content if $external_link->generic_msg;
      $return_data{generic_msg_label} = $external_link->generic_msg->label if $external_link->generic_msg;
      $return_data{theme} = 'external-link';
    }
    debug Dumper \%return_data;
    return to_json( \%return_data );
  };

  # /api/areamap/
  # Processes AJAX requests from a location guides pages
  # on our public website
  ajax['get'] => 'areamap/:id[Int]' => sub {
    # retrieve the id from the route
    my $id = route_parameters->get('id');
    # bail if this is undef or otherwise empty
    # ADD code here for error checking and use "send_error" if needed

    # At this point, we have a valid id, so let's engage the
    # database (e.g. model) 
    

    # create a simple hash(ref) with data to return
    my $result = {
      foo => 'bar',
    };
    return to_json $result;
  };
};

1;
