package locguide::Collection;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'locguide';
use Dancer2::Plugin::LocGuideCommonData;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Deferred;

# uncomment this when AJAX processing is needed
#use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;

prefix '/collection' => sub {
  hook before_template_render => sub {
    my $tokens = shift;

    info "hook before_template_render called in /collection handler...";
    return unless request->path =~ /^\/collection\// && $tokens->{collection};

    my @sublibraries = schema->resultset('LocationguideSublibrary')->all;
    
    my @collectiongroups = schema->resultset('LocationguideCollectiongroup')
      ->search( { sublibrary_id => $tokens->{collection}->sublibrary_id } );
      
    my @othergroups = schema->resultset('LocationguideCollectiongroup')
      ->search( { sublibrary_id => { '!=', $tokens->{collection}->sublibrary_id } } );

    my @externallinks = schema->resultset('LocationguideExternallink')->all;
    my @areamaps = schema->resultset('LocationguideAreamap')
      ->search( undef, { order_by => [qw/building area/] } );

    $tokens->{sublibraries} = \@sublibraries;
    $tokens->{collectiongroups} = \@collectiongroups;
    $tokens->{othergroups} = \@othergroups;
    $tokens->{areamaps} = \@areamaps;
    $tokens->{externallinks} = \@externallinks;
  };

  get '/new' => sub {
    var app_section => 'collections';
    var app_section_title => 'Collections';
    my $collection = schema->resultset('LocationguideCollection')->new_result({});

    send_error "Unable to prepare a new collection record for entry!", 500
      unless $collection;

    var page_title => sprintf('New Collection Record');

    # signal whether this Collection is using the default location mapping
    # from it's assigned Sublibrary, or using overriden values
    $collection->{override_default_locmap} = ($collection->external_link || $collection->map);
    
    push @{vars->{body_class}}, 'collection';    
    
    # context data lists (sublibraries, etc) are fetched in the 
    # 'before_template_render' (above)
    template 'collection', {
      collection => $collection,
      'c' => $collection,
    };
  };

  post '/new' => sub {
    
    my $form = body_parameters->as_hashref_mixed;
    
    # remove the map and external link keys from the form
    # when not overriding the Sublibrary's defaults.
    $form->{external_link_id} = undef
      if !exists $form->{override_default_locmap} || $form->{external_link_id} eq '';
      
    $form->{map_id} = undef
      if !exists $form->{override_default_locmap} || $form->{map_id} eq '';
      
    delete $form->{override_default_locmap};
    
    my $collection = schema->resultset("LocationguideCollection")->new_result( $form );
    send_error "Unable to prepare new collection record for saving!" unless $collection;
   
    if (!$collection->insert) {
      send_error "Unable to insert new collection record!", 500;
    }
    
    deferred success => "Your changes have been saved.";
            
    # return to the edit form
    redirect '/collection/' . $collection->id;

  };
   
  get '/:id/delete' => sub {
    my $collection = schema->resultset('LocationguideCollection')->find( { id => var('id') } );
    send_error sprintf("Unable to find Collection record (id: %s)", var('id')), 404
      unless $collection;
    
    send_error sprintf("Unable to delete Collection (id: %s)", var('id')), 500
      if !$collection->delete;
      
    deferred success => "Collection removed successfully";
    
    redirect '/collections';
  };

  get '/:id' => sub {
    var app_section => 'collections';
    var app_section_title => 'Collections';
    my $collection_id = route_parameters->get('id');
    
    my $collection = schema->resultset('LocationguideCollection')
      ->find({ id => $collection_id });

		my $log_object_name = "Collection: " . $collection->code;
		$log_object_name .= " (" . $collection->label . ")" if $collection->label;

    #my $auth_user = schema->resultset('AuthUser')->find( { username => session('user')->{username} } );
    #$auth_user->related_resultset('admin_logs')->create({
    #	action_time 		=> var('now_stamp'),
    #	user_id					=> $auth_user->id,
    #	content_type_id => 291,
    #	object_id				=> var('id'),
    #	object_repr			=> $log_object_name,
    # create_message  => '',
    #});
    
    if ( !$collection ) {
      send_error "Unable to locate collection for (" . $collection_id . ")", 404;
    }
    var page_title => sprintf('Collection: %s: %s', $collection->code, $collection->label);

    # signal whether this Collection is using the default location mapping
    # from it's assigned Sublibrary, or using overriden values
    $collection->{override_default_locmap} = ($collection->external_link || $collection->map);
    
    push @{vars->{body_class}}, 'collection';    
        
    # context data lists (sublibraries, etc) are fetched in the 
    # 'before_template_render' (above)
    template 'collection', {
      collection => $collection,
      'c' => $collection,
    };
  };

  post '/:id' => sub {
    my $id = route_parameters->get('id');
    
    my $form = body_parameters->as_hashref_mixed;
    my $collection = schema->resultset("LocationguideCollection")->find( { id => $id } );
    send_error "Unable to locate collection record (id = " . $id . ")", 403 unless $collection;
    
    # remove the map and external link keys from the form
    # when not overriding the Sublibrary's defaults.
    $form->{external_link_id} = undef
      if !exists $form->{override_default_locmap} || $form->{external_link_id} eq '';
      
    $form->{map_id} = undef
      if !exists $form->{override_default_locmap} || $form->{map_id} eq '';
      
    delete $form->{override_default_locmap};
    
    if (!$collection->update( $form )) {
      send_error "Unable to save changes for collection (id = " . $id . ")", 500;
    }
    
    deferred success => "Your changes have been saved.";
            
    # return to the edit form
    redirect '/collection/' . route_parameters->get('id');
  };
};

prefix '/collections' => sub {
  get '' => sub {
    my $filter_collcode = query_parameters->get('collection_code')
      || query_parameters->get('ccode')
      || undef;
    
    my $filter_sublibrary = query_parameters->get('sublibrary')
      || query_parameters->get('s')
      || undef;

    my $collection_group_id = query_parameters->get('cg')
      || query_parameters->get('collection_group')
      || undef;

    my $filter = {};
    $filter->{code} = {'like', sprintf("%s\%", $filter_collcode)} if $filter_collcode;
    $filter->{collection_group_id} = $collection_group_id if $collection_group_id;
    $filter->{sublibrary_id} = $filter_sublibrary if $filter_sublibrary;

    my $collectiongroup = $collection_group_id 
      ? schema->resultset('LocationguideCollectiongroup')->find( { id => $collection_group_id } )
      : undef;
    
    my $collections = schema->resultset('LocationguideCollection')
        ->search( $filter, { 
            page => var('page'),
            rows => var('rows'),
            order_by => var('page_settings')->{order_by},
          } );

    var search_filter => $filter;
    debug Dumper $filter;

    # retrieve the resultset's pager
    # (for pagination display)
    my $resultset_pager = $collections->pager;
    
    var('page_settings')->{page_title} = $collectiongroup ? sprintf("%s Collections", $collectiongroup->description) : 'Collections';
    push @{vars->{body_class}}, 'collections';
    
    template 'collections', {
      collections => $collections,
      collectiongroup => $collectiongroup,
      pager => $resultset_pager,
    }

  };
};

1;

__END__


