package locguide::ExternalLink;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'locguide';
use Dancer2::Plugin::LocGuideCommonData;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Deferred;

# uncomment this when AJAX processing is needed
#use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;

prefix '/externallink' => sub {

	get '/new' => sub {
    var app_section => 'externallinks';
    var app_section_title => 'External Links';
    my $resultset = schema->resultset('LocationguideExternallink');
    my $externallink = $resultset->new_result( {} );
    
    var page_title => 'New External Link';
	  
	  my @genericmessages = schema->resultset('LocationguideGenericmessage')->all;
	  
	  template 'externallink', {
		  genericmessages => \@genericmessages,
		  externallink => $externallink,
		  'e' => $externallink,
	  };
	};

	post '/new' => sub {
    my $form = body_parameters->as_hashref_mixed;
    $form->{id} = undef;
    $form->{content} = $form->{content} || "";
   
    my $externallink = schema->resultset('LocationguideExternallink')
      ->find_or_create( $form, { key => 'primary' } );
    
    send_error( "Unable to create a new External Link", 500 ) if !$externallink;
    
    deferred success => "Successfully created new External Link";
    
    # return to the edit form
    redirect '/externallink/' . $externallink->id;
	};
   
  get '/:id/delete' => sub {
    my $externallink = schema->resultset('LocationguideExternallink')->find( { id => var('id') } );
    send_error sprintf("Unable to find External Link record (id: %s)", var('id')), 404
      unless $externallink;
    
    send_error sprintf("Unable to delete External Link (id: %s)", var('id')), 500
      if !$externallink->delete;
      
    deferred success => "External Link removed successfully";
    
    redirect '/externallinks';
  };
  
	get '/:id' => sub {   
    var app_section => 'externallinks';
    var app_section_title => 'External Links';
    my $externallink = schema->resultset('LocationguideExternallink')
      ->find({ id => var('id') });
    
	  if ( !$externallink ) {
		  send_error "Unable to locate external link for (" . var('id') . ")", 404;
	  }
		var page_title => sprintf('External Link: %s - %s', $externallink->title, $externallink->url);
	  
	  my @genericmessages = schema->resultset('LocationguideGenericmessage')->all;
	 
    my $message_content = $externallink->content;
    if ($externallink->generic_msg->id) {
      $message_content = $externallink->generic_msg->content;
    }
	  template 'externallink', {
		  genericmessages => \@genericmessages,
		  externallink => $externallink,
      message_content => $message_content,
		  'e' => $externallink,
	  };
	};

	post '/:id' => sub {
	  my $form = body_parameters->as_hashref_mixed;

    my $externallink = schema->resultset('LocationguideExternallink')->find( { id => var 'id' } );
   
    if (!$externallink->update( $form ) ) {
      # calling "send_error" will cause this route handler to stop at this point
      send_error "Unable to save changes for external link (id = " . var('id') . ")", 500;
    }
    
    # saving/updating was successful.
    deferred success => "Your changes were saved.";
    
    # return to the edit form
    redirect '/externallink/' . route_parameters->get('id');
  };
};

prefix '/externallinks' => sub {
  get '' => sub {
    
	  my $externallinks = schema->resultset('LocationguideExternallink')->search(
		  undef,
		  {
        page => var('page'),
        rows => var('rows'),
        order_by => var('page_settings')->{order_by},
		  }
	  );
    my $pager = $externallinks->pager;
  
	  template 'externallinks', {
		  externallinks => $externallinks,
		  pager => $pager,
		  activepage => 'externallinks',
	  };
  };
};

1;

__END__

get '/externallink/new' => sub {

	my $genericmessages = dul_genericmessages();

	my $accept = request->header('accept');
	if ($accept =~ /json/) {
		content_type 'application/json';
		return to_json( { objectlabel => 'External Link', genericmessages => $genericmessages } );
	}	
};

get '/externallink/:id' => sub {
	my $id = route_parameters->get('id');
	my $sql = "SELECT * FROM locationguide_externallink WHERE id = ?";

	my $sth = database->prepare( $sql );
	$sth->execute( $id );
	my $externallink = $sth->fetchrow_hashref();

	my $genericmessages = dul_genericmessages();

	my $accept = request->header( 'accept' );
	if ($accept =~ /json/) {
		content_type 'application/json';
		return to_json( { 
			display => $externallink, 
			objectlabel => 'External Link', 
			genericmessages => $genericmessages, 
		} );
	}

	template 'externallink', { externallink => $externallink };	
};


