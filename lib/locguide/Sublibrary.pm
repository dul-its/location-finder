package locguide::Sublibrary;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'locguide';
use Dancer2::Plugin::LocGuideCommonData;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Deferred;

# uncomment this when AJAX processing is needed
#use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;

prefix '/sublibrary' => sub {
  get '/new' => sub {
    var app_section => 'sublibraries';
    var app_section_title => 'Sublibraries';
  };
  
  post '/new' => sub {
  };
   
  get '/:id/delete' => sub {
    my $sublibrary = schema->resultset('LocationguideSublibrary')->find( { id => var('id') } );
    send_error sprintf("Unable to find Sublibrary record (id: %s)", var('id')), 404
      unless $sublibrary;
    
    send_error sprintf("Unable to delete Sublibrary (id: %s)", var('id')), 500
      if !$sublibrary->delete;
      
    deferred success => "Sublibrary removed successfully";
    
    redirect '/sublibraries';
  };
  
  get '/:id' => sub {
    var app_section => 'sublibraries';
    var app_section_title => 'Sublibraries';
    my $sublibrary_id = route_parameters->get('id');
    
    my $sublibrary = schema->resultset('LocationguideSublibrary')
      ->find({ id => $sublibrary_id });
    
	  if ( !$sublibrary ) {
		  send_error "Unable to locate sublibrary for (" . $sublibrary_id . ")", 404;
	  }
		var page_title => sprintf('Sublibrary: %s - %s', $sublibrary->code, $sublibrary->label);
	  
	  my @locationmaps = schema->resultset('LocationguideLocationmap')->all;
	  
	  template 'sublibrary', {
		  locationmaps => \@locationmaps,
		  sublibrary => $sublibrary,
		  's' => $sublibrary,
	  };
    
  };
  
  post '/:id' => sub {
	  my $form = body_parameters->as_hashref_mixed;
	  
    my $sublibrary = schema->resultset('LocationguideSublibrary')->find( { id => var 'id' } );
    if (!$sublibrary->update( $form ) ) {
    
      # calling "send_error" will cause this route handler to stop at this point
      send_error "Unable to save changes for sublibray (id = " . var('id') . ")", 500;
    }
    
    # saving/updating was successful.
    deferred success => "Your changes were saved.";
    
    # return to the edit form
    redirect '/genericmessage/' . route_parameters->get('id');
  };
};

prefix '/sublibraries' => sub {
  
  get '' => sub {
    my $sublibraries = schema->resultset('LocationguideSublibrary')
      ->search(undef, { 
            page => var('page'),
            rows => var('rows'),
            order_by => var('page_settings')->{order_by},
        });
    my $pager = $sublibraries->pager;
    
    var page_title => 'Sublibraries';
    
    template 'sublibraries', {
   		activepage => 'sublibraries',
      pager => $pager,
      sublibraries => $sublibraries,
    }; 
  };
  
};

1;

__END__


