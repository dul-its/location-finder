package locguide::Typeahead;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'locguide';
use Dancer2::Plugin::LocGuideCommonData;
use Dancer2::Plugin::Database;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;

# Typeahead lookup for call numbers
ajax '/callno-typeahead/:q' => sub {
  my $q = route_parameters->get('q');
  $q = lc($q);

  my $sql = "SELECT lm.id, lm.callno_range as 'name', '' as code, CONCAT_WS(' ', lm.callno_range, " .
            "CONCAT_WS('', '(', lm.callno_start,'-',lm.callno_end,')')) as label, 'Location Map' as refobject, 'locationmap' as slug, " .
            "'Location Map' as reftype " .
            "FROM locationguide_locationmap lm " .
            "WHERE (INSTR(LOWER(lm.callno_range), ?) > 0) " .
            "   OR (INSTR(LOWER(lm.callno_start), ?) > 0) " .
            "   OR (LOWER(lm.callno_start) <= ? AND LOWER(lm.callno_end) >= ?) " .
            "ORDER BY label";
  my $lm_sth = database->prepare( $sql );
  $lm_sth->execute( $q, $q, $q, $q );
  my $results = $lm_sth->fetchall_arrayref( {} );
  to_json( $results );
};

get '/search-typeahead/:q' => sub {
	my $q = route_parameters->get('q');
	content_type 'application/json';

	my $sql = "SELECT z.id, z.name, z.code, z.label, z.refobject, z.slug, z.reftype FROM " .
						"(" .
						"SELECT cg.id, cg.description as 'name', '' as code, cg.description as label, 'Collection Group' as refobject, 'collectiongroup' AS slug, 'Collection Group' as reftype " .
						"FROM locationguide_collectiongroup cg " .
						"WHERE INSTR(LOWER(cg.description), ?) > 0 " .
						"UNION " .
						"SELECT c.id, c.label as 'name', c.code, c.label, 'Collection' as refobject, 'collection' AS slug, 'Collection' as reftype " .
						"FROM locationguide_collection c " .
						"WHERE INSTR(LOWER(c.code), ?) > 0 " .
				 		"   OR INSTR(LOWER(c.label), ?) > 0 " .
						"UNION " .
						"SELECT s.id, s.label as 'name', s.code, s.label, 'Sublibrary' as refobject, 'sublibrary' AS slug, 'Sublibrary' as reftype " .
						"FROM locationguide_sublibrary s " .
						"WHERE INSTR(LOWER(s.code), ?) > 0 " .
				 		"   OR INSTR(LOWER(s.label), ?) > 0 " .
				 		"UNION " .
				 		"SELECT lm.id, lm.callno_range as 'name', NULL as code, CONCAT_WS(' ', lm.callno_range, " . 
            "CONCAT_WS('', '(', lm.callno_start,'-',lm.callno_end,')')) as label, 'Call No. Location Map' as refobject, 'locationmap' as slug, " .
            "'Call No. Location Map' as reftype " .
            "FROM locationguide_locationmap lm " .
            "WHERE INSTR(LOWER(lm.callno_range), ?) > 0 " .
            "   OR INSTR(LOWER(lm.callno_start), ?) > 0 " .
            "UNION " .
            "SELECT lm.id, cg.description as 'name', NULL as code, cg.description as label, " . 
            "'Location Map' as refobject, 'locationmap' as slug, " .
            "'Location Map' as reftype " .
            "FROM locationguide_locationmap lm " .
            "LEFT JOIN locationguide_collectiongroup cg ON cg.id = lm.collection_group_id " .
            "WHERE INSTR(LOWER(cg.description), ?) > 0 " .
						") z " .
						"ORDER BY z.label ";

	my $cg_sth = database->prepare( $sql );
	$cg_sth->execute( $q, $q, $q, $q, $q, $q, $q, $q );
	my $results = $cg_sth->fetchall_arrayref( {} );
  debug Dumper $results;
	to_json( $results );
};

1;
__END__
