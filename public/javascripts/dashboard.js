$(function() {

	$('.dropzone', '.areamaps').on("addedfile", function(file) {
		alert('here');
	});

	/* BUTTON actions */
	$('#building-accordion').on('shown.bs.collapse', function(evt) {
		var e = evt;
		var target = $(evt.target).attr('data-active-building');
		
		$.ajax({
			url : '/building-link-clicked',
			data : { building : target },
			type : 'POST',
			dataType : 'json',
			cache : false,
			success : function(data, txtStatus, o) {
				var d = data;
			},
			error : function(o, txtStatus, errThrown) {
				var d = errThrown;
			}
		});
	});
	
	if( $('#selGenericMessage').length ) {
		if( $('option:selected', "#selGenericMessage").val() != "") {
			$('#txtContent').prop('disabled', true)
		}
	}
	$('#selGenericMessage').on('change', function(e) {
		var genmsg_id = $("option:selected", this).val();
		var want_generic_msg = (genmsg_id != "")
		$('#txtContent').prop('disabled', want_generic_msg);
		if (want_generic_msg) {
			$.getJSON('/genericmessage/' + genmsg_id + '/content', {}, function(data) {
				$('#txtContent').val(data.content);
			});
		} else {
			$('#txtContent').val("");
		}
	});

	$('.locationmaps-row')
		.on('hidden.bs.collapse', function(e) {
			var id = $(this).attr('id');
			$('[data-target="#' + id + '"]').text('Show');
		})
		.on('shown.bs.collapse', function(e) {
			var id = $(this).attr('id');
			$('[data-target="#' + id + '"]').text('Hide');
		});

	$('button[data-trigger="collgroup"]').on('click', function() {
		if ($(this).hasClass('show-collgroup')) {
			$(this)
				.html('Hide')
				.removeClass('show-collgroup')
				.addClass('hide-collgroup');

			// close any open rows..
			$('TR.collgroups-row').hide();

		} else {
			$(this)
				.html('Show')
				.removeClass('hide-collgroup')
				.add('show-collgroup');
		}

		var dataTarget = $(this).attr('data-target');
		$(dataTarget).fadeToggle();

	});

    // LocationMap Maps and External Link toggle
    $('#selAreaMap').on('change', function() {
        if ($(this).val() != "" && $('#selExternalLink').size() > 0) {
            // reset any "same-level" external link menu
            $('#selExternalLink').val("");
        }
    });
    $('#selExternalLink').on('change', function() {
        if ($(this).val() != "" && $('#selAreaMap').size() > 0) {
            $('#selAreaMap').val("");
        }
    });

    // LocationMap Maps and External Link toggle
    $('#selSAreaMap').on('change', function() {
        if ($(this).val() != "" && $('#selSExternalLink').size() > 0) {
            // reset any "same-level" external link menu
            $('#selSExternalLink').val("");
        }
    });
    $('#selSExternalLink').on('change', function() {
        if ($(this).val() != "" && $('#selSAreaMap').size() > 0) {
            $('#selSAreaMap').val("");
        }
    });

	var initFavoriteLink = function(is_favorite) {
		if (is_favorite != undefined) {
			var favstarClass = 'glyphicon glyphicon-star', favLinkText = ' Favorite';
			if (is_favorite == 1) {
				favstarClass = favstarClass + '-empty';
				favLinkText = 'Remove' + favLinkText;
			} else {
				favLinkText = 'Add' + favLinkText;
			}
			$('.toggle-favorite')
				.find('.glyphicon')
					.addClass(favstarClass)
					.end()
				.find('.favLinkText')
					.text(favLinkText);
		}
	}

	var initFormControls = function(object_type) {
		// Some controls need to be disabled when another dependent control
		// has a legitimate (truthy) value...
		$('[data-disable-control]', '.form-' + object_type).each(function(n, el) {
			var control = $(this).attr('data-disable-control');
			var truthy = $(this).val() != "" && $(this).val() != "NULL" && $(this).val() != "null";
			$(control).prop("disabled", truthy);

			$(this).change(function(e) {
				var control = $(this).attr('data-disable-control');
				var truthy = $(this).val() == "" || $(this).val() == "NULL" || $(this).val() == "null";
				$(control).prop("disabled", !truthy);
			});
		});

		$('.has-dependent-object', '.form-' + object_type).hide();

		$('SELECT.has-create-object', '.form-' + object_type).change(function(e) {
			var $option = $(this).find("option:selected");
			if ( $option.is('.create-dependent-object') ) {

				var exposeControl = $option.attr('data-dependent-form');
				$(exposeControl).show();

				return e.preventDefault();
			}
		})
	}
	
	$('#overrideDefaultLocmap', 'BODY.collection').on('click', function(e) {
	  var relControl = $(this).attr('id');
	  $('[data-rel-control="#' + relControl + '"]').prop('disabled', function(i, v) {
	    // restore the default selected values when the fieldset 
	    // is currently not disabled
	    if (!v) {
	      $('SELECT', this).each(function(e, z) {
	        $(this).val( $(this).attr('data-default-id') ); 
	      });
	    }
	    return !v; 
	   });
	});
	$('[data-toggle-selvalue]').on('change', function(evt) {
	  if ( $(this).val() != '' ) {
  	  $( $(this).attr('data-toggle-selvalue') ).val('');
  	}
	});

	// SEARCH TYPEAHEAD
	// --------------------------------------------------------------
	$('.search-typeahead').typeahead({
		minLength	: 2,
		items : 20,
		fitToElement: true,
		autoSelect: true,
		source: function(q, process) {
			var unix = Math.round(+new Date()/1000);
			return $.getJSON(
				'/search-typeahead/' + q + '?' + unix,
				function (data) {
					return process(data);
				});
		},
		displayText: function(item) {
		  if (item.code == null || item.code == "") {
		    return item.name + " (" + item.reftype + ")";
		  }
			return "[" + item.code + "] " + item.name + " (" + item.reftype + ")";
		},
		afterSelect: function(current) {
			// otherwise, go to the location.
			window.location.href = '/' + current.slug + '/' + current.id;
		}
	});
	
	// SEARCH TYPEAHEAD
	// --------------------------------------------------------------
	$('.callno-typeahead').typeahead({
		minLength	: 2,
		items : 20,
		fitToElement: true,
		autoSelect: true,
		source: function(q, process) {
			var unix = Math.round(+new Date()/1000);
			return $.getJSON(
				'/callno-typeahead/' + q + '?' + unix,
				function (data) {
					return process(data);
				});
		},
		displayText: function(item) {
			return item.name + " (" + item.reftype + ")";
		},
		afterSelect: function(current) {
			// otherwise, go to the location.
			window.location.href = '/' + current.slug + '/' + current.id;
		}
	});

	$('.broadcast-collection-event').on('change', function(e) {
		var event = jQuery.Event('collection-checkbox-clicked', { selector : '.broadcast-collection-event' } );
		$( document ).trigger( event );
	});

	//----------- DOCUMENT events -------------//
	$( document ).on( 'collection-checkbox-clicked', function(e) {
		$('.collection-action').prop('disabled', $(e.selector + ':checked').size() == 0);
	});

	//----------- MULTISELECT ----------------//
	$('button.multiselect').on('click', function(evt) {
		var origin = '.' + $(this).attr('data-origin');
		var target = '.' + $(this).attr('data-target');

		// exit if there is no selected option(s) on the origin element
		if ($('option:selected', origin).size == 0) {
			return;
		}

    var selected = $('OPTION:selected', origin);
    
    // the 'target' select list contains an <OPTGROUP>
    // element to store the "newly assigned (or added)" items
    var optgroupNew = $('OPTGROUP.newitems', target);
    
    if (optgroupNew.size()) {
      // moving/adding related items to list
      $('option:contains("None added")').remove();
      selected.detach().appendTo( optgroupNew );
    } else {
    
      // removing "assigned" items, adding back to the 
      // 'all' list
      selected.detach().prependTo( $(target) );
      
      // Sort the list of available items
      var options = $(target).find('option');
      options.sort(function(a, b) { return $(a).text() > $(b).text() ? 1 : -1 });
      $(target).html('').append(options);
    }

    return evt.preventDefault();
	});

	// MASONRY GRID for areamaps
  var $grid = $('.grid').masonry({
  	itemSelector: '.grid-item',
		columnWidth: '.grid-sizer',
		percentPosition: true
  });
  // layout Masonry after each image loads
  $grid.imagesLoaded().progress( function() {
  	$grid.masonry('layout');
  });
 
    $('#btnCollGroupApply').on('click', function(e) {
        url = $(this).attr('href');
        sublibrary = $('#filterSublibrary').val();
        description = $('#filterDescription').val();
        url = url + '?sublibrary=' + sublibrary + '&description=' + description;
        location.href = url;
        return e.preventDefault();
    });

    $('#btnCollectionsApply').on('click', function(e) {
        url = $(this).attr('href');
        collection_code = $('#filterCollectionCode').val();
        sublibrary = $('#filterSublibrary').val();
        url = url + '?sublibrary=' + sublibrary + '&ccode=' + collection_code;
        location.href = url;
        return e.preventDefault();
    });
});
