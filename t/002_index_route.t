use strict;
use warnings;

use locguide;
use Test::More tests => 2;
use Plack::Test;
use HTTP::Request::Common;

my $app = locguide->to_app;
is( ref $app, 'CODE', 'Got app' );

my $test = Plack::Test->create($app);
my $res  = $test->request( GET '/' );

my %data = ( collection_code => 'PLI', sublibrary => 'LILLY', callno => 'PN1995 .E52 1967');
my %data_no_callno = ( collection_code => 'PLI', sublibrary => 'LILLY');

my $response = HTTP::Request::Common::POST
    '/mapinfo',
    Content_Type => 'x-www-form-urlencode',
    Content => [ %data ],
;


ok( $res->is_success, '[GET /] successful' );
