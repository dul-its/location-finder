#!/bin/bash

cd /app
starman -l 0.0.0.0:5000 --workers 5 -E ${PLACK_ENV:-development} -I/app bin/app.psgi

