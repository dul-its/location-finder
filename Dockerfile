FROM image-mirror-prod-registry.cloud.duke.edu/library/perl:latest

# having all of the Development Tools is likely overkill, but
# we don't want the CPANM install processes to run into any 
# "gotchas" around the absense of the build tools...

# Included so as to have 'netstat' and 'which' (useful when debugging)
# RUN apt update && apt-get install -y net-tools mariadb

# RUN apt-get install -y libxml2-dev libcurl4-openssl-dev libyaml-dev
# All of the known parts for Perl and Dancer2

USER 0

RUN apt update

ADD ./cpanfile .
RUN cpanm -f -v --notest --no-interactive --installdeps .

# expose port 5000 for which we'll run "plackup"
EXPOSE 5000
EXPOSE 5001

# I don't know why I added this previously
ENV container docker

WORKDIR /app
COPY ./ .

COPY ./entrypoint.sh /usr/local/bin/entrypoint.sh

USER 1001

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["start"]
