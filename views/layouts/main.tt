<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="<% settings.charset %>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <title><% IF vars.page_title %><% vars.page_title %><% ELSE %><% settings.appname %><% END %></title>

	<!--script src="https://kit.fontawesome.com/d9ab24eaaa.js" crossorigin="anonymous"></script-->
	<script src="https://kit.fontawesome.com/240d2dc48b.js" crossorigin="anonymous"></script>

  <!-- Dropzone File Uploading -->
  <link href="/css/dropzone.min.css" rel="stylesheet">
	
	<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"-->
  <link rel="stylesheet" href="/css/lightbox.min.css">
	<link rel="stylesheet" href="/css/dulcet.style.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/dashboard.css">
	<% IF extracss.size %>
	<% FOREACH css IN extracss %>
	<link rel="stylesheet" <% FOREACH attr IN css.pairs %><% attr.key %>="<% attr.value %> "<% END %> />
	<% END %>
	<% END %>
</head>
<body class="<% body_class %>">
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#locguide-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/"><% settings.appname %></a>
    </div>
    <div class="collapse navbar-collapse" id="locguide-navbar-collapse-1">
    	<p class="navbar-text">Hello, <% auth_user.given_name || auth_user.username %></p>
      <ul class="nav navbar-nav navbar-right">
      	<%# loop of links here, maybe (with 'active' indicator?) %>
      	<% FOREACH nl IN settings.ui.nav_links %>
      	<li<% IF activepage == nl.name %> class="active"<% END %>><a href="<% nl.path %>"><% nl.text %><% IF activepage == nl.name %> <span class="sr-only">(current)</span><% END %></a></li>
      	<% END %>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other <span class="caret"></span></a>
          <ul class="dropdown-menu">
						<li><a href="<% request.uri_for( '/collections' ) %>">Collections</a></li>
            <li><a href="#">Upload Map</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">About</a></li>
          </ul>
        </li>
      </ul>
    </div>
	</div>
</nav>
<div class="<% settings.ui.container_class %>">
	<div class="row">
		<div class="h2 col-sm-7">
			<div class="btn-toolbar">
				<a role="button" href="/" class="btn btn-success btn-sm">
					<i class="fa fa-home"></i>&nbsp;</a>
				<div class="btn-group">
					<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Create New...&nbsp;<span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="<% request.uri_for( '/locationmap/new' ) %>">Location Map</a></li>
						<li><a href="<% request.uri_for( '/collection/new' ) %>">Collection</a></li>
						<li><a href="<% request.uri_for( '/collectiongroup/new' ) %>">Collection Group</a></li>
						<li><a href="<% request.uri_for( '/externallink/new' ) %>">External Link</a></li>
						<li><a href="<% request.uri_for( '/genericmessage/new' ) %>">Generic Message</a></li>
					</ul>
        </div>
        <% IF vars.nav_context_buildings.size %>
        <div class="btn-group">
          <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Switch Libraries...&nbsp;<span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
          <% FOR b in vars.nav_context_buildings %>
            <li><a href='<% request.uri_for( "/locationmaps/${b}" ) %>'><% b %></a></li>
          <% END %>
          </ul>
				</div>
        <% END %>
				<a href="#" data-toggle="modal" class="btn btn-sm btn-primary disabled" data-dataset-url="/areamap/new" data-objecttype="areamap">
					<span class="glyphicon glyphicon-upload"></span>&nbsp;Upload Map Image
				</a>
			</div>			
		</div>
		<div class="h2 col-sm-5">
			<div class="input-group input-group-sm">
				<input type="text" data-provide="typeahead" autocomplete="off" class="form-control search-typeahead input-sm" placeholder="Search for Sublibraries, Collection Groups or Collections...">
				<div class="input-group-btn">
				  <button type="button" class="btn btn-info btn-sm">Recent...</button>
				  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <span class="caret"></span>
				    <span class="sr-only">Toggle Dropdown</span>
				  </button>
				  <ul class="dropdown-menu dropdown-menu-right">
				  	<% IF vars.auth_user && vars.auth_user.admin_logs %>
				  	<% FOREACH rlog IN vars.auth_user.admin_logs.reverse.slice(0,9) %>
				  	<li><a href="<% request.uri_for("${rlog.content_type.model}/${rlog.object_id}") %>"><% rlog.object_repr %></a></li>
				  	<% END %>
				  	<% ELSE %>
				  	<li class="disabled"><a href="#">No History Yet</a></li>
				  	<% END %>
				    <li role="separator" class="divider"></li>
				    <li><a href="#">Separated link</a></li>
				  </ul>
				</div>
			</div>
		</div>
	</div>
</div>

<% IF deferred.success %>
<div class="<% settings.ui.container_class %>">
	<div class="alert alert-success">
	<div><strong>Success</strong></div>
	<p><% deferred.success %></p>
	</div>
</div>
<% END %>

<% IF deferred.error %>
<div class="<% settings.ui.container_class %>">
	<div class="alert alert-danger">
		<div><strong>Error</strong></div>
		<p><% deferred.error %></p>
	</div>
</div>
<% END %>

<% IF session.show_validation_alert %>
<div class="<% settings.ui.container_class %>">
	<div class="alert alert-warning">
	<p><% IF session.validation_alert_title %><strong><% session.validation_alert_title %></strong>&nbsp;<% END %>
	<% session.validation_alert_message %></p>
	</div>
</div>
<% END %>

<% INCLUDE 'breadcrumbs.tt' %>

<div class="<% settings.ui.container_class %>">
	<div class="row">
    <% IF vars.page_settings.page_title %>
		<div class="col-sm-12 h2"><% vars.page_settings.page_title %></div>
    <% ELSE %>
    <div class="col-sm-12 h2"><% vars.page_title %></div>
    <% END %>
	</div>
</div>

<% content %>

<footer>
	<div class="<% settings.ui.container_class %>">

	</div>
</footer>

<!-- Grab jQuery from a CDN, fall back to local if necessary -->
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript">/* <![CDATA[ */
    !window.jQuery && document.write('<script type="text/javascript" src="/javascripts/jquery.js"><\/script>')
/* ]]> */</script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="/javascripts/lightbox.min.js"></script>
<script src="/javascripts/dulcet.script.js"></script>
<script src="/javascripts/bootstrap3-typeahead.min.js"></script>
<script src="/javascripts/dropzone.min.js"></script>
<script src="/javascripts/dashboard.js"></script>
<script src="/javascripts/locationmap.js"></script>

<% IF extrajs.size %>
<% FOREACH js IN extrajs %>
	<script <% FOREACH attr IN js.pairs %><% attr.key %>="<% attr.value %>" <% END %>></script>
<% END %>
<% END %>

</body>
</html>
