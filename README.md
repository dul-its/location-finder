Location Guide (or Map Finder) web application project.

[[_TOC_]]
# TL;DR
```
$ git clone git@gitlab.oit.duke.edu:dul-its/location-finder.git

$ cd /path/to/location-finder

# you'll need a database sql file
$ wget -O django_local.sql https://automation.lib.duke.edu/locationguide/django_production_latest.sql

$ cp docker-compose.sample.yml docker-compose.yml

$ docker-compose up --build -d

# want to see the logs?
$ docker logs -f locationguide_app
```
  
Please allow 5-10 minutes while all of the dependencies install.  
Afterwards, you should be able to visit http://localhost:5000

# How Location Guide (Map Finder) Fits in DUL Ecosystem
There are 3 servers involved in processing a "Where Is It Located" request by a web user.  
* `find.library.duke.edu` - the Discovery Catalog web application (dul-argon-skin) server
* `library.duke.edu` - the DUL website (aka "The Drupal Server")
* `locationguide.lib.duke.edu` - the "Location Guide" server (which hosts this project), providing the `/mapinfo` API endpoint

### The workflow looks like this:
```mermaid
sequenceDiagram
    Client Browser->>+Discovery Server: User clicked "Where to Find It?" Icon
    Discovery Server->>+DUL Server: Request for /mapinfo endpoint
    DUL Server->>+Map Finder Server: Forward request (via proxy)
    Map Finder Server->>-DUL Server: Send JSON response
    DUL Server->>-Discovery Server: Receive JSON response
    Discovery Server->>-Client Browser: Display Popup Modal with answer
```
1. User, visiting `find.library.duke.edu` (Discovery Catalog), clicks "Map Finder" icon when viewing an item.  
2. The web request reaches `https://library.duke.edu/mapinfo`.  
3. The web server for `library.duke.edu` forwards the request (via proxypass) to the Location Guide server (`locationguide.lib.duke.edu/mapinfo`)  
4. Location Guide processes the request, and returns JSON-formatted data.  
5. The Discovery Catalog receives the JSON-formatted data and displays either a map or message in a pop-up window.  

# Developer Notes
## Creating Localized Configuration File
```bash
$ cp -r docs/environments environments
```
Open `environments/development.yml` and adjust your database settings.  
  
For example:
```yaml
plugins:
  DBIC:
    default:
      dsn: dbi:mysql:database=mydatabase;host=myhost;port=3306
      user: myuser
      password: r3daCtedP4ass
      schema_class: yourapp::Schema
      options:
        RaiseError: 1
        PrintError: 1
```
  
`schema_class` is explained in a bit more detail in the [docs](docs/schema.md).
  
**Note**: It's recommended that you use the environment variable, `DBI_DSN` when using the Docker container setup.  
  
### Project Directory Structure
```bash
├── bin
├── config.yml
├── cpanfile
├── docs
├── lib
├── Makefile.PL
├── MANIFEST
├── MANIFEST.SKIP
├── public
├── README.md
├── t
└── views
```

**lib** contains the main controller module -- locguide.pm -- which processes requests for the root endpoint and utililty endpoints.    
Route handlers for the separate entities (e.g. Collection, Collection Group, etc) are located in the **locguide** directory under **lib**.  
  
In addition to route handler "controllers", the various model files are located at `lib/locguide/Schema`.  
  
Static files, CSS and Javascript files are located under **public**.

## Sandbox Instance
Visit https://locguide-pre.lib.duke.edu to see work-in-progress on the new Location Guide.

## Database Notes
The application runs on top of a MariaDB (or MySQL) database.  
The sandbox database is located at **mariadb-stage-01.lib.duke.edu**.  
The intended production database will be located at **mariadb-01.lib.duke.edu**.

## Creating  systemd Service (Running a daemon)
To run as a daemon service, you can create a **systemd** service file. A [very useful example service file](./docs/systemd/locationguide.service) has been included for your convenience.  
Simply replace `/path/to/location-finder` with the actual location, then copy the file to `/etc/systemd/system`.  
  
Example:  
```bash
$ sudo cp /path/to/modified/locationguide.service /etc/systemd/service
$ sudo systemctl enable locationguide
$ sudo systemctl start locationguide
```
