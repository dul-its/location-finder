Location Guide (or Map Finder) web application project.

[[_TOC_]]

## Sandbox Instance
Visit https://locguide-pre.lib.duke.edu to see work-in-progress on the new Location Guide.

## Getting Started - Clone Project

## Getting Started - After Cloning Project
Getting started with developing (and/or testing) Location Guide requires:
* Creating an 'environment-specific' config file
* Verifying Perl modules
  
### Creating an 'environment-specific' config file
The project ships with a `config.yml` file for common settings, but it does not specify:
* Database connection settings
* Session engine

Create a folder named "environments" at the root of the project, then create a file named "development.yml". This will contain 
all of your custom configuration, and will provide a space for database specs (see below).
  
#### Database Connection Settings
Database connection is facilatated by the "Dancer2::Plugin::DBIC" module. Add this to your 
environment-specific configutation:  
  
For example, running in a "development" setup:  
```yaml
plugins:
  DBIC:
    default:
      dsn: dbi:mysql:database=mydatabase;host=myhost;port=3306
      user: myuser
      password: r3daCtedP4ass
      schema_class: yourapp::Schema
      options:
        RaiseError: 1
        PrintError: 1
```
We'll explain `schema_class` in a bit.  
  
## Routes and Database Interaction
### Route Handlers
Just like a typical Rails app, Dancer2 apps consist of route handlers, responding to HTTP requests.  
  
Let's define a quick handler for a `/locationmaps` route, and this can be seen in [LocationMap.pm](lib/locguide/LocationMap.pm):
```perl
package locguide::LocationMap;
use strict;
...
use Dancer2 appname => 'locguide';
# other use lines
...
prefix 'locationmaps' => sub {
    get '' => sub {
        my $locationmaps = schema->resultset('LocationguideLocationamp')->search(...);
        template 'locationmaps', {
            locationmaps => $locationmaps,
        }
    };
};
...
### other routes/etc
```
### Database Interaction

##### Querying Database Is Simple
##### DBIx::Class::Schema::Loader
It is highly recommending to install [DBIx::Class::Schema::Loader](https://metacpan.org/pod/DBIx::Class::Schema::Loader) which provides **dbicdump**, a utility that will generate *.pm files for all of you database tables in your schema.
#### Session Engine
If Memcached or Redis are installed on your system, you can configure either for your session engine. For instance, 
let's configure Memcached:
```
session: Memcached
engines:
  session:
    Memcached:
      memcached_serves: 127.0.0.1:11211,/var/sock/memcached
```
