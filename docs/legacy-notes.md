Location Guide (or Map Finder) web application project.

[[_TOC_]]
# TL;DR
```
$ git clone git@gitlab.oit.duke.edu:dul-its/location-finder.git

$ cd /path/to/location-finder

# you'll need a database sql file
$ wget -O django_local.sql https://automation.lib.duke.edu/locationguide/django_production_latest.sql

$ cp docker-compose.sample.yml docker-compose.yml

$ docker-compose up --build -d

# want to see the logs?
$ docker logs -f locationguide_app
```
  
Please allow 5-10 minutes while all of the dependencies install.  
Afterwards, you should be able to visit http://localhost:5000

# How Location Guide (Map Finder) Fits in DUL Ecosystem
There are 3 servers involved in processing a "Where Is It Located" request by a web user.  
* `find.library.duke.edu` - the Discovery Catalog web application (dul-argon-skin) server
* `library.duke.edu` - the DUL website (aka "The Drupal Server")
* `locationguide.lib.duke.edu` - the "Location Guide" server (which hosts this project), providing the `/mapinfo` API endpoint

### The workflow looks like this:
```mermaid
sequenceDiagram
    Client Browser->>+Catalog Server: User clicked "Where to Find It?" Icon
    Catalog Server->>+DUL Server: Request for /mapinfo endpoint
    DUL Server->>+Map Finder Server: Forward request (via proxy)
    Map Finder Server->>-DUL Server: Send JSON response
    DUL Server->>-Catalog Server: Receive JSON response
    Catalog Server->>-Client Browser: Display Popup Modal with answer
```
1. User, visiting `find.library.duke.edu` (Discovery Catalog), clicks "Map Finder" icon when viewing an item.  
2. The web request reaches `https://library.duke.edu/mapinfo`.  
3. The web server for `library.duke.edu` forwards the request (via proxypass) to the Location Guide server (`locationguide.lib.duke.edu/mapinfo`)  
4. Location Guide processes the request, and returns JSON-formatted data.  
5. The Discovery Catalog receives the JSON-formatted data and displays either a map or message in a pop-up window.  

# Developer Notes
## Understanding Perl
We've created a small Perl Primer to help wrap your head around the language:  
[Derrek's Perl Primer for ITS Devs/DevOps](https://gitlab.oit.duke.edu/devops/techdocs/-/tree/master/perl)

## Getting Started - Development
### Install cpanm
More than likely, Perl is installed on your workstation, and just as likely that **cpan** is, too.  However, it is 
recommend that you install **cpanm** to make your Perl module management life easier.  
  
Assiming **cpan** is already installed:
```bash
$ cpan App::cpanminus
```
### Quick Start
This method involves cloning the project locally and creating a localized configuration file. Optionally, you can initialize a local MySQL (or MariaDB) database.  
#### Cloning Project
```bash
$ cd /path/to/your/workspace

# this step assumes you have SSH or PGP keys in place
$ git clone git@gitlab.oit.duke.edu:dul-its/location-finder.git
$ cd /path/to/your/workspace/location-finder
```
#### Creating Localized Configuration File
```bash
$ cp -r docs/environments environments
```
Open `environments/development.yml` and adjust your database settings.  
  
For example:
```yaml
plugins:
  DBIC:
    default:
      dsn: dbi:mysql:database=mydatabase;host=myhost;port=3306
      user: myuser
      password: r3daCtedP4ass
      schema_class: yourapp::Schema
      options:
        RaiseError: 1
        PrintError: 1
```
`schema_class` is explained in a bit more detail in the [docs](docs/schema.md).

**If running a local MySQL (or MariaDB) instance**  
* Be sure to grant permissions:

  ```mysql
  GRANT ALL ON locationguide.* TO 'locationguide'@'localhost' IDENTIFIED BY 'your-fav-secure-pw'
  ```
  
* Import the data located at `docs/sql/django.sampledata.sql`

  ```bash
  $ mysql -u<your-admin-user> -p < docs/sql/django.sampledata.sql
  ``` 
  
**If you prefer a remote host containing data**
Use `mariadb-dev-01.lib.duke.edu` for the host name.  
Contact Core Services to get the password for the `locationguide` user.

##### Configuring Session Engine
If Memcached or Redis are installed on your system, you may configure either as your session engine.  
For instance, let's configure Memcached:
```yaml
session: Memcached
engines:
  session:
    Memcached:
      memcached_serves: 127.0.0.1:11211,/var/sock/memcached
```
  
#### Install Perl Module Dependencies
With **cpanm** installed (see above), run this command:
```bash
$ cpanm -S --installdeps .
```
...where `-S` means run in "sudo" mode. You may need to enter your 'sudo' password. Or...
```bash
$ sudo cpanm --installdeps .
```

#### Start the application in "Dev" mode
You should now be able to start the web application by typing the following:  
```bash
$ plackup -p 5000 bin/app.psgi
```
Read more about `plackup` here (https://metacpan.org/pod/Plack).

### Project Directory Structure
```bash
├── bin
├── config.yml
├── cpanfile
├── docs
├── lib
├── Makefile.PL
├── MANIFEST
├── MANIFEST.SKIP
├── public
├── README.md
├── t
└── views
```

**lib** contains the main controller module -- locguide.pm -- which processes requests for the root endpoint and utililty endpoints.    
Route handlers for the separate entities (e.g. Collection, Collection Group, etc) are located in the **locguide** directory under **lib**.  
  
In addition to route handler "controllers", the various model files are located at `lib/locguide/Schema`.  
  
Static files, CSS and Javascript files are located under **public**.

## Sandbox Instance
Visit https://locguide-pre.lib.duke.edu to see work-in-progress on the new Location Guide.

## Database Notes
The application runs on top of a MariaDB (or MySQL) database.  
The sandbox database is located at **mariadb-stage-01.lib.duke.edu**.  
The intended production database will be located at **mariadb-01.lib.duke.edu**.

## Creating  systemd Service (Running a daemon)
To run as a daemon service, you can create a **systemd** service file. A [very useful example service file](./docs/systemd/locationguide.service) has been included for your convenience.  
Simply replace `/path/to/location-finder` with the actual location, then copy the file to `/etc/systemd/system`.  
  
Example:  
```bash
$ sudo cp /path/to/modified/locationguide.service /etc/systemd/service
$ sudo systemctl enable locationguide
$ sudo systemctl start locationguide
```
