requires "Dancer2" => "0.400001";
requires "Dancer2::Plugin::Ajax";
requires "Dancer2::Plugin::Database";
requires "Dancer2::Plugin::Deferred";
requires "Dancer2::Plugin::DBIC";
requires "Dancer2::Serializer::JSON";
requires "Dancer2::Session::Memcached";
requires "DateTime";
requires "DateTime::Set";
requires "DBD::MariaDB";
requires "DBIx::Class::ResultSet";
requires "DBIx::Class::Result::Validation";
requires "DBIx::Class::Storage::DBI::MariaDB";
requires "ExtUtils::Embed";
requires "Fatal";
requires "Module::Build::Tiny";
requires "Number::Format";
requires "Plack";
requires "Plack::Middleware::CrossOrigin";
requires "Plack::Test";
requires "Set::Infinite";
requires "TAP::Harness::Env";
requires "Task::Plack";
requires "XML::LibXML";
requires "XML::SAX";
requires "XML::Simple";

recommends "Slack";
recommends "DBIx::Class::Schema::Loader";
recommends "YAML"             => "0";
recommends "URL::Encode::XS"  => "0";
recommends "CGI::Deurl::XS"   => "0";
recommends "HTTP::Parser::XS" => "0";

on "test" => sub {
    requires "Test::More"            => "0";
    requires "HTTP::Request::Common" => "0";
};
