#!/bin/bash

HOST=${1:-https://locationguide-stage.lib.duke.edu}
echo "HOST = ${HOST}"

echo "[mapinfo -- AJAX POST] Locating 'The Year That Broke Politics'"
echo "sending data via POST"
curl -d '{"collection_code":"PKW","sublibrary":"PERKN","callno":"JK526"}' \
  -X POST "$HOST/api/mapinfo" \
  -H 'content-Type: application/json'
echo ""
echo "..."
echo ""

echo "[api/mapinfo -- application/json] test that includes all parameters:"
echo "sending data via POST"
curl -d '{"collection_code":"PLI","sublibrary":"LILLY","callno":"PN1995 .E52 1967"}' \
  -X POST "$HOST/api/mapinfo" \
  -H 'content-Type: application/json'
echo ""
echo "...."
echo ""

echo "[api/mapinfo -- application/json] test that omits call number:"
echo "sending data via POST"
curl -d '{"collection_code":"PLI","sublibrary":"LILLY"}' \
  -X POST "$HOST/api/mapinfo" \
  -H 'content-Type: application/json'
echo ""
echo "...."
echo ""

echo "[mapinfo] sending data via AJAX POST"
curl -d '{"collection_code":"PLI","sublibrary":"LILLY","callno":"PN1995 .E52 1967"}' \
  -X POST "$HOST/mapinfo" \
  -H 'Content-Type: application/json' \
  -H 'X-Requested-With: XMLHttpRequest'
echo ""
echo "----------------"

echo "[mapinfo] sending data via AJAX POST (no call number)"
curl -d '{"collection_code":"PLI","sublibrary":"LILLY"}' \
  -X POST "$HOST/mapinfo" \
  -H 'Content-Type: application/json' \
  -H 'X-Requested-With: XMLHttpRequest'
echo ""
echo "----------------"

echo "[mapinfo] sending data as 'x-www-form-urlencoded' POST"
curl -d "collection_code=PLI&sublibrary=LILLY&callno=PN1995 .E52 1967" \
  -X POST "$HOST/mapinfo" \
  -H 'content-Type: application/x-www-form-urlencoded'
echo  ""
echo "----------------"

echo "[api/areamap] requesting areamap data..."
curl -X GET "$HOST/api/areamap/71" \
  -H 'content-Type: application/json' \
  -H 'X-Requested-With: XMLHttpRequest'
