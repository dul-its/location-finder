# Location Guide on OKD

## Checklist
* Create Helm chart at `helm-chart/app`
* Create database secret (if using mariadb, postgres, etc)
* Copy `issuer.yaml` and `route.yaml` from working projects (e.g. R2T2)
  * Use `sed` for string replacement, ensuring that `app` appears instead of other project names.
* Verify persistent volume claims, requesting needed PVs from OIT (Nate Childers)
* Complete work on templates
* Create appropriate CNAME record
* Integrate `oauth2` and manage groups
