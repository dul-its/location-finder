{{/*
Expand the name of the chart.
*/}}
{{- define "app.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "app.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "app.labels" -}}
helm.sh/chart: {{ include "app.chart" . }}
{{ include "app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Volume name for "assets"
*/}}
{{- define "app.assets-volume" -}}
volume-dul-locationguide-assets
{{- end }}

{{/*
Volume name for "data(store)"
*/}}
{{- define "app.data-volume" -}}
volume-dul-locationguide-datastore
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "app.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "app.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the fullname of the database
*/}}
{{- define "mariadb.fullname" -}}
{{- printf "%s-mariadb" .Release.Name }}
{{- end }}

{{/*
Name of the secret resource where passwords are kept
*/}}
{{- define "mariadb.secretname" -}}
{{- printf "%s-%s" .Chart.Name "mariadb" | trunc 63 | trimSuffix "-" -}}
{{- end }}

{{/*
Create the fullname of the memcached container
*/}}
{{- define "memcached.fullname" -}}
{{- printf "%s-%s" .Release.Name "memcached" | trunc 63 | trimSuffix "-" -}}
{{- end }}

{{- define "app.databaseUser" }}
locguide
{{- end -}}

{{- define "app.databaseName" }}
locguide
{{- end }}

{{- define "app.databasePassword" -}}
{{ $dbPass := (include "getValueFromSecret" (dict "Namespace" .Release.Namespace "Name" "app-mariadb" "Key" "mariadb-password")) -}}
{{ print (trim $dbPass) }}
{{- end -}}

{{/*
Get value from a secret
TIP SOURCE: 
https://stackoverflow.com/questions/50452665/import-data-to-config-map-from-kubernetes-secret
*/}}
{{- define "getValueFromSecret" -}}
{{- $len := (default 16 .Length) | int -}}
{{- $obj := (lookup "v1" "Secret" .Namespace .Name).data -}}
{{- if $obj }}
{{- index $obj .Key | b64dec -}}
{{- else -}}
{{- randAlphaNum $len -}}
{{- end -}}
{{- end -}}

{{- define "app.databaseDSN" -}}
{{- $dbUser := (include "app.databaseUser" .Context ) -}}
{{- $dbName := (include "app.databaseName" .Context ) -}}
{{ $dbHost := (include "mariadb.fullname" .Context ) -}}
{{ printf "dbi:mysql:database=%s;host=%s;port=3306" ((trim $dbName) (trim $dbHost) (trim $dbName)) }}
{{- end }}
