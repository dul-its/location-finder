#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";

use locguide;
use Plack::Builder;

builder {
    enable 'CrossOrigin', origins => '*';

    # enable static file handling of "/images/media", typically not 
    # present under the project's "/public" folder.
    #
    # See: https://metacpan.org/pod/release/MIYAGAWA/Plack-0.9974/lib/Plack/Middleware/Static.pm#CONFIGURATIONS
    # regarding the munging of $_ in a subroutine in order to 
    # produce the correct path for 'root'
    enable "Plack::Middleware::Static",
        path => sub { s!^/images/media/!! }, root => locguide->config->{media_path} . '/';

    # after handling all static paths that require attention, 
    # pass the PSGI app instance
    locguide->to_app;
}

# When not using the Builder setup, uncomment this line
#locguide->to_app;
