#!/bin/bash

set -e

# Assumptions for when this file is reached (called)
# 1) We have not logged into "oc"
# 2) The following variables are present:
#    - RELEASE_VALUES_OPTION (either --reset-values or --reuse-values)
#    - HELM_RELEASE_NAME
#    - IMAGE_NAME
#
#    ..plus all CI "predefined" variables
#    and this project's CI variables.

oc login $K8S_API_URL --token=$APP_TOKEN

oc project $K8S_NAMESPACE

echo "Let's see our working directory"
pwd

cd chart/app

echo "building 'helm upgrade' command..."
echo "RELEASE_VALUES_OPTION = [$RELEASE_VALUES_OPTION]"
HELM_CMD="helm upgrade --install $HELM_RELEASE_NAME . "
HELM_CMD+="--kube-apiserver $K8S_API_URL "
HELM_CMD+="$RELEASE_VALUES_OPTION --values values-$HELM_RELEASE_NAME.yaml "

# when we're using '--reset-values' we must include the values from db.yaml
# as well as the sp cert and key
if [ "$RELEASE_VALUES_OPTION" == "--reset-values" ]; then
  HELM_CMD+="--values db.yaml --values memcached.yaml "
  HELM_CMD+="--set duke-oauth-proxy.oidc.client.secret=$OIDC_SECRET --set duke-oauth-proxy.oidc.client.token=$OIDC_TOKEN "
fi

# finally, append the image tag
HELM_CMD+="--set image.tag=$IMAGE_NAME"

# run the command
$HELM_CMD

